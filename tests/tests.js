let TodoApp = require('./todoApp');
let mockData = {
    text1: {
        desc: 'Testing for New Items',
        date: '12 Jun, 17',
        tags: 'important,critical'
    },
    text2: 'Testing for Counter update',
    text3: {
        desc: 'Testing for Editable Items',
        date: '12 Jul, 17',
        tags: 'Important,Critical,Urgent'
    },
    text4: 'Testing for Delete Btn',
    text5: {
        desc: 'Testing for expired item',
        date: '12 Jul, 17',
        tags: ''
    },
    text6: {
        desc: 'Testing for item Due today',
        date: '25 Jul, 17',
        tags: 'Urgent'
    },
    emptyString: '',
    alertText: 'You didn\'t enter anything'
}

describe('Todo App', () => {
    browser.ignoreSynchronization = true; // This allows protractor to run on regular website, not specific to angular 

    browser.get('http://localhost:8080/');
    browser.wait(() => element(by.id('todo-list')).isPresent(), 10000);

    let todoApp = new TodoApp();

    it('should display a list of todos', () => {
        expect(todoApp.ListIsDisplayed()).toBe(true);
    });

    it('should display a form to add new items by clicking on a button', () => {
        todoApp.addItemBtn.click();
        expect(todoApp.addItemForm.isPresent()).toBe(true);
    });

    it('should display three inputs in form along with submit button', () => {
        expect(todoApp.itemDesc.isPresent()).toBe(true);
        expect(todoApp.itemDueDt.isPresent()).toBe(true);
        expect(todoApp.itemTags.isPresent()).toBe(true);
        todoApp.addItemModal.element(by.css('.close')).click();
        browser.sleep('1000');
    });

    it('should be able to add new todo items & list should grow as items get added', () => {
        todoApp.itemCount().then((lastCount) => {
            todoApp.addItem(mockData.text1.desc, mockData.text1.date, mockData.text1.tags);
            expect(todoApp.lastItem('getText')).toEqual(mockData.text1.desc);
            expect(todoApp.itemCount()).toEqual(lastCount + 1);
        });
    });

    it('should not allow empty inputs', () => {
        todoApp.itemCount().then((lastCount) => {
            todoApp.addItem(mockData.emptyString, '', '');
            expect(todoApp.itemCount()).toEqual(lastCount);
            todoApp.addItemModal.element(by.css('.close')).click();
            browser.sleep('1000');
        });

    });

    it('should be able to toggle items between Completed and incompleted states', () => {
        todoApp.lastItem('clearBtn').click();
        expect(todoApp.hasClass(todoApp.lastItem(), 'cleared')).toBe(true);
        todoApp.lastItem('clearBtn').click();
        expect(todoApp.hasClass(todoApp.lastItem(), 'cleared')).toBe(false);
    });

    it('should update the remaining Items counter as new items get added', () => {
        todoApp.activeItemsCount().then((lastActiveCount) => {
            todoApp.addItem(mockData.text2, '', '');
            expect(todoApp.activeItemsCount()).toEqual((parseInt(lastActiveCount) + 1).toString());
        });

    });

    it('should filter the list when a filter button is clicked', () => {
        todoApp.firstItem('clearBtn').click();
        todoApp.itemCount().then((count) => {
            todoApp.filterBtn('active').click();
            expect(todoApp.itemCount()).toEqual(count - 1);
        })
        todoApp.filterBtn('completed').click();
        todoApp.itemCount().then((count) => {
            todoApp.clearedItemsCount().then((clearedCount) => {
                expect(todoApp.itemCount()).toEqual(clearedCount);
            })
        })
        todoApp.filterBtn('all').click();
    });

    it('should not display clear Completed button if there are no Completed todos', () => {
        todoApp.firstItem('clearBtn').click();
        expect(todoApp.clearCompletedBtn().isDisplayed()).toBe(false);
    });

    it('should remove all Completed tasks when clear Button is pressed', () => {
        todoApp.itemCount().then((lastCount) => {
            todoApp.firstItem('clearBtn').click();
            todoApp.clearCompletedBtn().click();
            expect(todoApp.itemCount()).toEqual(lastCount - 1);
        });
    });

    it('should provide a delete btn for all individual tasks, only visible on hover', () => {
        expect(element.all(by.css('.delete')).first().isDisplayed()).toBe(false);
        todoApp.addItem(mockData.text4, '', '');
        todoApp.hoverOnFirstItem();
        expect(todoApp.firstItem('deleteBtn').isDisplayed()).toBe(true);
        let thirdItem = todoApp.itemByIndex(1).element(by.css('.desc')).getText(); //saving second item's text
        todoApp.firstItem('deleteBtn').click(); //delete first item
        expect(todoApp.firstItem('getText')).toEqual(thirdItem); //check if the second item has taken the first place
    });

    it('provides a checkbox to toggle all todos between complete & incomplete states', () => {
        todoApp.selectAllBtn().click();
        todoApp.itemCount().then((count) => {
            expect(todoApp.clearedItemsCount()).toEqual(count); //check if all are selected
        });
        todoApp.selectAllBtn().click();
        expect(todoApp.clearedItemsCount()).toEqual(0); //check if none are selected
    });

    it('Complete all todos button should check itself if all todos are completed manually', () => {
        todoApp.clickAllItems();
        expect(todoApp.hasClass(element(by.id('select-all-icon')), 'green')).toBe(true);
    });

    it('allows the user to doubleclick a todo to edit it\'s content', () => {
        todoApp.doubleClickFirstItem() //double clicking on first Todo item.
        expect(todoApp.firstItemEditForm().isDisplayed()).toBe(true);
    });

    it('should persist the edited text when enter is pressed', () => {
        todoApp.editFirstItem(mockData.text3);
        expect(todoApp.firstItem('getText')).toEqual(mockData.text3.desc);
        expect(todoApp.firstItem('getTags')).toEqual(mockData.text3.tags);
        expect(element.all(by.id('due-date')).first().getText()).toEqual(mockData.text3.date);
    });

    it('should highlight items due or expired', () => {
        todoApp.addItem(mockData.text5.desc, mockData.text5.date, mockData.text5.tags);
        expect(todoApp.hasClass(todoApp.lastItem(), 'light-red')).toBe(true);
        todoApp.addItem(mockData.text6.desc, mockData.text6.date, mockData.text6.tags);
        expect(todoApp.hasClass(todoApp.lastItem(), 'light-green')).toBe(true);
    })

    it('should be able to filter tasks based on tags', () => {
        todoApp.tags.get(1).click();
        expect(todoApp.itemCount()).toEqual(1);
        todoApp.tags.get(1).getText().then((btnText) => {
            todoApp.firstItem('getTags').then((tagText) => {
                expect(tagText.search(btnText)).not.toBeLessThan(0);
                todoApp.tags.get(0).click();
            });
        });
    })

    it('should persist user data between application reloads', () => {
        todoApp.itemCount().then((count) => {
            browser.refresh();
            browser.wait(() => element(by.id('todo-list')).isPresent(), 10000);
            expect(todoApp.itemCount()).toEqual(count);
        });
    });


});

//browser.sleep('3000'); delays any script after it for 3s.

//Useful Tutorial on Protractor
//http://blog.scottlogic.com/2015/11/06/ProtractorForBeginnersPart1.html