let TodoApp = function() {
    let allTodoItems = element.all(by.css('li.list-group-item'));
    let itemsLeft = element(by.id('items-left')).element(by.css('.badge'));
    let filterBtns = element.all(by.css('.filter-btns'));
    let clearCompletedBtn = element(by.id('clear-completed'));
    this.tags = element(by.id('tag-filters')).all(by.css('a'));
    this.addItemBtn = element(by.id('add-item-btn'));
    this.addItemForm = element(by.id('add-item-form'));
    this.itemDesc = element(by.id('todo-desc'));
    this.itemDueDt = element(by.id('todo-due-dt'));
    this.itemTags = element(by.css('.ui-autocomplete-input'));
    this.itemSubmitBtn = element(by.id('todo-submit'));
    this.addItemModal = element(by.id('add-item-modal'));
    this.hasClass = function(element, cls) {
        return element.getAttribute('class').then(function(classes) {
            return classes.split(' ').indexOf(cls) !== -1;
        });
    };
    this.ListIsDisplayed = function() {
        return element(by.id('todo-list')).isDisplayed();
    }
    this.addItem = function(desc, date, tags) {
        this.addItemBtn.click();
        browser.sleep('1000');
        this.itemDesc.clear().sendKeys(desc);
        this.itemDueDt.clear().sendKeys(date);
        this.itemTags.clear().sendKeys(tags);
        this.itemSubmitBtn.click();
        browser.sleep('1000');
    }
    this.lastItem = function(mode) {
        if (mode === 'getText') {
            return allTodoItems.last().element(by.css('.desc')).getText();
        }
        if (mode === 'getDate') {
            return allTodoItems.last().element(by.id('due-dt')).getText();
        }
        if (mode === 'getTags') {
            return allTodoItems.last().element(by.css('.tags')).getText();
        }
        if (mode === 'clearBtn') {
            return element.all(by.css('.clear-toggle')).last();
        }
        if (mode === 'deleteBtn') {
            return element.all(by.css('.delete')).last();
        }
        return allTodoItems.last();
    }
    this.firstItem = function(mode) {
        if (mode === 'getText') {
            return allTodoItems.first().element(by.css('.desc')).getText();
        }
        if (mode === 'getDate') {
            return allTodoItems.first().element(by.id('due-dt')).getText();
        }
        if (mode === 'getTags') {
            return allTodoItems.first().element(by.css('.tags')).getText();
        }
        if (mode === 'clearBtn') {
            return element.all(by.css('.clear-toggle')).first();
        }
        if (mode === 'deleteBtn') {
            return element.all(by.css('.delete')).first();;
        }
        return allTodoItems.first();
    }
    this.itemByIndex = function(index) {
        return allTodoItems.get(index);
    }
    this.itemCount = function() {
        return allTodoItems.count();
    }
    this.activeItemsCount = function() {
        return itemsLeft.getText();
    }
    this.clearedItemsCount = function() {
        return element.all(by.css('li.list-group-item.cleared')).count();
    }
    this.filterBtn = function(btn) {
        if (btn === 'active') {
            return filterBtns.get(1);
        }
        if (btn === 'completed') {
            return filterBtns.get(2);
        }
        if (btn === 'all') {
            return filterBtns.get(0);
        }
    }
    this.clearCompletedBtn = function() {
        return clearCompletedBtn;
    }
    this.hoverOnFirstItem = function() {
        browser.actions().mouseMove(allTodoItems.first()).perform();
    }
    this.doubleClickFirstItem = function() {
        browser.actions().doubleClick(allTodoItems.first()).perform();
    }
    this.selectAllBtn = function() {
        return element(by.id('select-toggle'));
    }
    this.clickAllItems = function() {
        //For loop used as .each() was not working and was returning an error about stale element.
        allTodoItems.count().then((count) => {
            for (var i = 0; i < count; i++) {
                allTodoItems.get(i).element(by.css('.clear-toggle')).click();
            }
        });
    }
    this.firstItemEditForm = function() {
        return this.firstItem().element(by.id('edit-form'));
    }
    this.editFirstItem = function(text) {
        let form = this.firstItemEditForm();
        element(by.id('edited-todo-desc')).clear().sendKeys(text.desc);
        element(by.id('edited-todo-due-dt')).clear().sendKeys(text.date);
        form.element(by.css('.ui-autocomplete-input')).clear().sendKeys(text.tags);
        element(by.id('edit-form-submit')).click();
        //form.sendKeys(protractor.Key.ENTER)
    }
    this.getAlert = function() {
        return browser.switchTo().alert();
    }
}


module.exports = TodoApp;