let updateList = require('../render/updateList');

function filterTodo(mode) {
    app.filter = mode;
    updateList();
}

module.exports = filterTodo;