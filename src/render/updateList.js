let service = require('../services/service');
let itemsLeft = require('../counters/itemsLeft');
let clearedCount = require('../counters/clearedCount');

function collectAllTags() {
    let DB = service.getDB();
    let tags = [];
    DB.forEach((item) => {
        item.tags.split(',').forEach((tag) => {
            tags.push(tag);
        });
    });
    tags = tags.filter((tag) => {
        return tag !== 'No Tags';
    });
    tags = tags.filter((item, index, self) => {
        return self.indexOf(item) === index;
    });
    return tags;
}

function generateRenderObject() {
    return {
        filteredDB: service.filteredDB(),
        leftItemsCount: itemsLeft(),
        clearedItemsCount: clearedCount(),
        tags: collectAllTags()
    };
}

function updateList() {
    let renderObject = generateRenderObject();
    let renderedApp = '';
    $('#app').empty();
    //handlebars template being used
    let generateApp = $('#generate-app').html();
    let template = Handlebars.compile(generateApp);
    renderedApp = template(renderObject);
    //handlebars template ends here
    $('#app').append(renderedApp);
    $('.datepicker').datepicker();
    $('.datepicker').datepicker("option", "dateFormat", 'd MM, y');
    $('#todo-tags').tagit({ placeholderText: 'Add Tags here' });
}


module.exports = updateList;