let service = require('./services/service');
let addNewItem = require('./controllers/addNewItem');
let clearCompleted = require('./controllers/clearCompleted');
let selectToggle = require('./controllers/selectToggle');
let editTodo = require('./controllers/editTodo');
let saveEdit = require('./controllers/saveEdit');
let deleteTodo = require('./controllers/deleteTodo');
let clearToggle = require('./controllers/clearToggle');
let updateList = require('./render/updateList');
let filterTodo = require('./filters/filterTodo');
let firebase = require('./model/firebase');

//Some Global variables exposed to window so that all functions can have access.
window.app.filter = '';
window.app.selectionMode = 'select';

//Binding functions to events

$(document).on('submit', '#add-item-form', (event) => {
    event.preventDefault();
    $('#add-item-modal .close').click();
    $('#add-item-modal').on('hidden.bs.modal', () => {
        addNewItem(event.currentTarget);
    });
});

$(document).on('click', '.delete', (e) => {
    deleteTodo(e.currentTarget);
    updateList();
});

$(document).on('click', '.clear-toggle', (e) => {
    clearToggle(e.currentTarget);
    updateList();
});

$(document).on('click', '#filter-all', () => {
    filterTodo('all');
});

$(document).on('click', '#filter-active', () => {
    filterTodo('active');
});

$(document).on('click', '#filter-completed', () => {
    filterTodo('completed');
});

$(document).on('click', '#clear-completed', () => {
    clearCompleted();
});

$(document).on('click', '#select-toggle', () => {
    selectToggle();
});

$(document).on('click', '#tag-filters a', (event) => {
    service.setTag(event.currentTarget.textContent);
    updateList();
});

$(document).on('submit', '#edit-form', (event) => {
    event.preventDefault();
    saveEdit(event);
    updateList();
});

/*$(document).on('focusout', '#edit-form', (event) => {
    saveEdit(event);
    updateList();
});*/

//editableTodo initited below.
$(document).on('dblclick', 'li', (event) => {
    editTodo(event);
    $('.datepicker').datepicker();
    $('.datepicker').datepicker("option", "dateFormat", 'd MM, y');
    $('#edited-todo-tags').tagit({ placeholderText: 'Add Tags here' });
});

//Calling updateList for initial rendering.

let ref = firebase.database().ref();
ref.on('value', function(snapshot) {
    service.setDB(snapshot);
    updateList();
});

//Save DB to localStorage before window close.

window.onbeforeunload = function() {
    service.saveDB();
}