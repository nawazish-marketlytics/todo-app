let firebase = require('../model/firebase');
let DB;

function checkDue(date) {
    let givenDate = Date.parse(date);
    let todayDate = Date.parse(new Date(new Date().setHours(0, 0, 0, 0)));
    let dueClass = '';
    if (givenDate < todayDate) {
        dueClass = 'light-red';
    } else if (givenDate === todayDate) {
        dueClass = 'light-green';
    }
    return dueClass;
}

function tagsHandler(tags) {
    if (tags === '') {
        return 'No Tags';
    } else {
        return tags;
    }
}

function addItemToDB(item) {
    let ref = firebase.database().ref();
    let key = ref.push().key;
    let newData = {
        id: key,
        todo: item.todoDesc,
        dueDt: item.todoDueDt,
        tags: tagsHandler(item.todoTags),
        cleared: '',
        dueClass: checkDue(item.todoDueDt)
    }
    ref.push(newData);
    //DB.push(newData);
}

function clearCompleted() {
    DB = DB.filter((todoObj) => {
        return todoObj.cleared === '';
    });
    saveDB();
}

function clearToggle(id) {

    DB.forEach((todoObj) => {
        if (todoObj.id === id) {
            if (todoObj.cleared === '') {
                todoObj.cleared = 'cleared';
            } else {
                todoObj.cleared = '';
            }
        }
    });
}

function deleteTodo(id) {
    DB.forEach((todoObj, index) => {
        if (todoObj.id === id) {
            DB.splice(index, 1);
        }
    });
    saveDB();
}

function saveEdit(event, id) {
    let form = event.currentTarget;
    DB.forEach((todoObj) => {
        if (todoObj.id === id) {
            todoObj.todo = form.editedTodoDesc.value;
            todoObj.dueDt = form.editedTodoDueDt.value;
            todoObj.tags = tagsHandler(form.editedTodoTags.value);
            todoObj.dueClass = checkDue(form.editedTodoDueDt.value);
        }
    });
    saveDB();
}

function selectToggle(icon) {
    if (app.selectionMode === 'select') {
        DB.forEach((todoObj) => {
            todoObj.cleared = 'cleared';
        });
        icon.toggleClass("green");
        app.selectionMode = 'unselect';
    } else {
        DB.forEach((todoObj) => {
            todoObj.cleared = '';
        });
        app.selectionMode = 'select';
        icon.toggleClass("green");
    }
}

function getDB() {
    return DB;
}

function setDB(snapshot) {
    DB = [];
    snapshot.forEach((snap) => {
        DB.push(snap.val());
    });
}

let tag = '';

function setTag(text) {
    tag = text;
}

function filterByTag() {
    let filteredDBByTag = DB;
    if (tag !== '' && tag !== 'All') {
        filteredDBByTag = DB.filter((todoObj) => {
            return todoObj.tags.search(tag) >= 0;
        });
    }
    return filteredDBByTag;
}

function filteredDB() {

    let filteredDB = filterByTag();
    if (app.filter === 'active') {
        filteredDB = filteredDB.filter((todoObj) => {
            return todoObj.cleared === '';
        });
    } else if (app.filter === 'completed') {
        filteredDB = filteredDB.filter((todoObj) => {
            return todoObj.cleared === 'cleared';
        });
    }
    return filteredDB;
}

function saveDB() {
    firebase.database().ref().set(DB);
}

module.exports = {
    addItemToDB: addItemToDB,
    clearCompleted: clearCompleted,
    clearToggle: clearToggle,
    deleteTodo: deleteTodo,
    saveEdit: saveEdit,
    selectToggle: selectToggle,
    getDB: getDB,
    setDB: setDB,
    filteredDB: filteredDB,
    saveDB: saveDB,
    setTag: setTag
}