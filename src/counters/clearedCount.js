function clearedCount() {
    let DB = require('../services/service').getDB();
    let items = DB.filter((todoObj) => {
        return todoObj.cleared === 'cleared';
    });
    if (items.length === DB.length && items.length !== 0) {
        app.selectionMode = 'unselect';
        return { color: 'green', clearBtn: 'visible', length: items.length };
    } else {
        app.selectionMode = 'select';
        if (items.length === 0) {
            return { color: '', clearBtn: '', length: items.length };
        }
        return { color: '', clearBtn: 'visible', length: items.length };
    }
}

module.exports = clearedCount;