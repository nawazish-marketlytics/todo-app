function itemsLeft() {
    let DB = require('../services/service').getDB();
    let items = DB.filter((todoObj) => {
        return todoObj.cleared === '';
    });
    return items.length;
}

module.exports = itemsLeft;