let firebase = require("firebase");

let config = {
    apiKey: "AIzaSyBRgNolCAh5DMYLdllgPhVJ5st1-GzFq0k",
    authDomain: "todo-app-6d4b1.firebaseapp.com",
    databaseURL: "https://todo-app-6d4b1.firebaseio.com",
    projectId: "todo-app-6d4b1",
    storageBucket: "todo-app-6d4b1.appspot.com",
    messagingSenderId: "462123823405"
};

firebase.initializeApp(config);

module.exports = firebase;