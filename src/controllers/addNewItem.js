let updateList = require('../render/updateList');
let service = require('../services/service');

function addNewItem(form) {
    let formObj = {
        todoDesc: form.todoDesc.value,
        todoDueDt: form.todoDueDt.value,
        todoTags: form.todoTags.value,
    };

    service.addItemToDB(formObj);
    updateList();
}

module.exports = addNewItem;