let DB = require('../model/database');

function idGen() {
    let id = DB[DB.length - 1].id + 1;
    return function() {
        return id++;
    }
}

let id = idGen();

function todoObj(todo) {
    this.todo = todo;
    this.cleared = '';
    this.id = id();
}

module.exports = todoObj;