let service = require('../services/service');

function editTodo(e) {
    let DB = service.getDB();
    let element = $(e.currentTarget); //$(this) was returning null Object, Hence a workaround used.
    let id = element.attr('data-id');
    let input = '';
    DB.forEach((todoObj) => {
        if (todoObj.id === id) {
            if (todoObj.tags === 'No Tags') {
                todoObj.tags = '';
            }
            //handlebars template being used
            let generateInput = $('#generate-edit-form').html();
            let template = Handlebars.compile(generateInput);
            input = template(todoObj);
            //handlebars template ends here
        }
    });
    element.empty().html(input);
    $('#edit').focus();
}

module.exports = editTodo;