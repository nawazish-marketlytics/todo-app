let service = require('../services/service');

function clearToggle(obj) {
    let id = obj.getAttribute('data-id');
    service.clearToggle(id);
}

module.exports = clearToggle;