let updateList = require('../render/updateList');
let service = require('../services/service');

function selectToggle() {
    let icon = $('#select-all-icon');
    service.selectToggle(icon);
    updateList();
}

module.exports = selectToggle;