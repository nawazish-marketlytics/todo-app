let service = require('../services/service');

function saveEdit(event) {
    let id = $(event.currentTarget).attr('data-id');
    service.saveEdit(event, id);
}

module.exports = saveEdit;