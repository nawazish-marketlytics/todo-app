let updateList = require('../render/updateList');
let service = require('../services/service');

function clearCompleted() {
    service.clearCompleted();
    updateList();
}

module.exports = clearCompleted;