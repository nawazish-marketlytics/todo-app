let service = require('../services/service');

function deleteTodo(obj) {
    let id = obj.getAttribute('data-id');
    service.deleteTodo(id);
}

module.exports = deleteTodo;