To start :

1. Clone the project
2. Run ```npm install```
3. Run ```npm start```
4. Go to ```localhost:8080``` on your browser.


To Test :

1. run ```npm install -g protractor```
2. run ```webdriver-manager update```
3. run ```webdriver-manager start```
