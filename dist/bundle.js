/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	__webpack_require__(1);
	
	__webpack_require__(2);

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	//DataBase;
	var DB = void 0;
	if (JSON.parse(localStorage.getItem('DB')) === null || JSON.parse(localStorage.getItem('DB')).length === 0) {
	    DB = [{ todo: 'Learn Ember.js', cleared: 'cleared', id: 0 }, { todo: '...', cleared: '', id: 1 }, { todo: 'Profit!', cleared: '', id: 2 }];
	} else {
	    DB = JSON.parse(localStorage.getItem('DB'));
	}
	console.log(JSON.parse(localStorage.getItem('DB')));
	//Object Constructor
	__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module \"./src/model/todoObj\""); e.code = 'MODULE_NOT_FOUND'; throw e; }()));
	//Controllers
	
	$('#todo-input').keypress(function (event) {
	    if (event.which === 13) {
	        if ($('#todo-input').val() !== '') {
	            var todo = new todoObj($("#todo-input").val());
	            DB.push(todo);
	            console.log(DB);
	            $("#todo-input").val('');
	            updateList();
	        } else {
	            alert('You didn\'t enter anything');
	        }
	    }
	});
	
	window.clearToggle = function (id) {
	    //Window used to expose functions globally for use inside html
	    DB.forEach(function (todoObj) {
	        if (todoObj.id === id) {
	            if (todoObj.cleared === '') {
	                todoObj.cleared = 'cleared';
	            } else {
	                todoObj.cleared = '';
	            }
	        }
	    });
	    updateList();
	};
	
	window.deleteTodo = function (id) {
	    //Window used to expose functions globally for use inside html
	    DB.forEach(function (todoObj, index) {
	        if (todoObj.id === id) {
	            DB.splice(index, 1);
	        }
	    });
	    updateList();
	};
	
	var filter = void 0;
	
	function filterTodo(mode) {
	    filter = mode;
	    updateList();
	}
	
	function itemsLeft() {
	    var items = DB.filter(function (todoObj) {
	        return todoObj.cleared === '';
	    });
	    $('#items-left .badge').html(items.length);
	}
	
	function clearedCount() {
	    var items = DB.filter(function (todoObj) {
	        return todoObj.cleared === 'cleared';
	    });
	    $('#items-cleared .badge').html(items.length);
	    if (items.length === DB.length) {
	        $('#select-all-icon').addClass('green');
	        selectionMode = 'unselect';
	    } else {
	        $('#select-all-icon').removeClass('green');
	        selectionMode = 'select';
	    }
	}
	
	function clearCompleted() {
	    DB = DB.filter(function (todoObj) {
	        return todoObj.cleared === '';
	    });
	    updateList();
	}
	
	var selectionMode = 'select';
	
	function selectToggle() {
	    var icon = $('#select-all-icon');
	    if (selectionMode === 'select') {
	        DB.forEach(function (todoObj) {
	            todoObj.cleared = 'cleared';
	        });
	        icon.toggleClass("green");
	        selectionMode = 'unselect';
	    } else {
	        DB.forEach(function (todoObj) {
	            todoObj.cleared = '';
	        });
	        selectionMode = 'select';
	        icon.toggleClass("green");
	    }
	    updateList();
	}
	
	function todoEditable() {
	    $('li').dblclick(function (e) {
	        var element = $(e.currentTarget); //$(this) was returning the whole document object weirdly, A workaround used.
	        var id = parseInt(element.attr('data-id'));
	        var input = '';
	        DB.forEach(function (todoObj) {
	            if (todoObj.id === id) {
	                input = '<div class="form-group">\n                    <input id="edit' + id + '" type="text" class="form-control" value="' + todoObj.todo + '">\n                    </div>';
	            }
	        });
	        element.empty().html(input);
	        $('#edit' + id).focus();
	        $('#edit' + id).keypress(function () {
	            if (event.which === 13) {
	                DB.forEach(function (todoObj) {
	                    if (todoObj.id === id) {
	                        todoObj.todo = $('#edit' + id).val();
	                    }
	                });
	                updateList();
	            }
	        });
	        $('#edit' + id).focusout(function () {
	            DB.forEach(function (todoObj) {
	                if (todoObj.id === id) {
	                    todoObj.todo = $('#edit' + id).val();
	                }
	            });
	            updateList();
	        });
	    });
	}
	
	function updateList() {
	    var filteredDB = void 0;
	    var updatedlist = '';
	    $('#todo-list').empty();
	    if (filter === 'active') {
	        filteredDB = DB.filter(function (todoObj) {
	            return todoObj.cleared === '';
	        });
	    } else if (filter === 'completed') {
	        filteredDB = DB.filter(function (todoObj) {
	            return todoObj.cleared === 'cleared';
	        });
	    } else {
	        filteredDB = DB;
	    }
	    filteredDB.forEach(function (todoObj) {
	        updatedlist += '\n        <li class="list-group-item ' + todoObj.cleared + '" data-id="' + todoObj.id + '">\n            <a href="#!" id="clear-toggle" class="todo-btn pull-left" onclick="clearToggle(' + todoObj.id + ')">\n                <i class="fa fa-check" aria-hidden="true"></i>\n            </a>\n            ' + todoObj.todo + '\n            <a href="#!" id="delete" class="todo-btn pull-right" onclick="deleteTodo(' + todoObj.id + ')">\n                <i class="fa fa-times" aria-hidden="true"></i>\n            </a>\n        </li>';
	    });
	    $('#todo-list').append(updatedlist);
	    itemsLeft();
	    clearedCount();
	    todoEditable();
	}
	
	//Binding functions to button click events
	
	$('#filter-all').click(function () {
	    filterTodo('all');
	});
	
	$('#filter-active').click(function () {
	    filterTodo('active');
	});
	
	$('#filter-completed').click(function () {
	    filterTodo('completed');
	});
	
	$('#clear-completed').click(function () {
	    clearCompleted();
	});
	
	$('#select-toggle').click(function () {
	    selectToggle();
	});
	updateList();
	
	window.onbeforeunload = function () {
	    localStorage.setItem('DB', JSON.stringify(DB));
	};

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(3);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(5)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../node_modules/css-loader/index.js!../node_modules/sass-loader/index.js!!./main.scss", function() {
				var newContent = require("!!../node_modules/css-loader/index.js!../node_modules/sass-loader/index.js!!./main.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(4)();
	// imports
	
	
	// module
	exports.push([module.id, ".todo-btn {\n  display: inline-block;\n  text-decoration: none;\n  color: grey;\n  text-align: center;\n  font-size: 15px; }\n\n#clear-toggle {\n  margin-right: 10px;\n  padding-right: 10px;\n  border-right: 1px solid grey; }\n\n#delete {\n  visibility: hidden;\n  color: red; }\n\nli:hover #delete {\n  visibility: visible; }\n\np {\n  margin: 0;\n  padding: 4px; }\n\n.mini-well {\n  padding: 4px; }\n\n.cleared {\n  text-decoration: line-through; }\n  .cleared #clear-toggle {\n    color: green; }\n\n#select-all-icon {\n  font-weight: 900; }\n\n.green {\n  color: green; }\n\n.flex {\n  display: flex;\n  justify-content: space-between; }\n", ""]);
	
	// exports


/***/ }),
/* 4 */
/***/ (function(module, exports) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	// css base code, injected by the css-loader
	module.exports = function() {
		var list = [];
	
		// return the list of modules as css string
		list.toString = function toString() {
			var result = [];
			for(var i = 0; i < this.length; i++) {
				var item = this[i];
				if(item[2]) {
					result.push("@media " + item[2] + "{" + item[1] + "}");
				} else {
					result.push(item[1]);
				}
			}
			return result.join("");
		};
	
		// import a list of modules into the list
		list.i = function(modules, mediaQuery) {
			if(typeof modules === "string")
				modules = [[null, modules, ""]];
			var alreadyImportedModules = {};
			for(var i = 0; i < this.length; i++) {
				var id = this[i][0];
				if(typeof id === "number")
					alreadyImportedModules[id] = true;
			}
			for(i = 0; i < modules.length; i++) {
				var item = modules[i];
				// skip already imported module
				// this implementation is not 100% perfect for weird media query combinations
				//  when a module is imported multiple times with different media queries.
				//  I hope this will never occur (Hey this way we have smaller bundles)
				if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
					if(mediaQuery && !item[2]) {
						item[2] = mediaQuery;
					} else if(mediaQuery) {
						item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
					}
					list.push(item);
				}
			}
		};
		return list;
	};


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	var stylesInDom = {},
		memoize = function(fn) {
			var memo;
			return function () {
				if (typeof memo === "undefined") memo = fn.apply(this, arguments);
				return memo;
			};
		},
		isOldIE = memoize(function() {
			return /msie [6-9]\b/.test(self.navigator.userAgent.toLowerCase());
		}),
		getHeadElement = memoize(function () {
			return document.head || document.getElementsByTagName("head")[0];
		}),
		singletonElement = null,
		singletonCounter = 0,
		styleElementsInsertedAtTop = [];
	
	module.exports = function(list, options) {
		if(true) {
			if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
		}
	
		options = options || {};
		// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
		// tags it will allow on a page
		if (typeof options.singleton === "undefined") options.singleton = isOldIE();
	
		// By default, add <style> tags to the bottom of <head>.
		if (typeof options.insertAt === "undefined") options.insertAt = "bottom";
	
		var styles = listToStyles(list);
		addStylesToDom(styles, options);
	
		return function update(newList) {
			var mayRemove = [];
			for(var i = 0; i < styles.length; i++) {
				var item = styles[i];
				var domStyle = stylesInDom[item.id];
				domStyle.refs--;
				mayRemove.push(domStyle);
			}
			if(newList) {
				var newStyles = listToStyles(newList);
				addStylesToDom(newStyles, options);
			}
			for(var i = 0; i < mayRemove.length; i++) {
				var domStyle = mayRemove[i];
				if(domStyle.refs === 0) {
					for(var j = 0; j < domStyle.parts.length; j++)
						domStyle.parts[j]();
					delete stylesInDom[domStyle.id];
				}
			}
		};
	}
	
	function addStylesToDom(styles, options) {
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			if(domStyle) {
				domStyle.refs++;
				for(var j = 0; j < domStyle.parts.length; j++) {
					domStyle.parts[j](item.parts[j]);
				}
				for(; j < item.parts.length; j++) {
					domStyle.parts.push(addStyle(item.parts[j], options));
				}
			} else {
				var parts = [];
				for(var j = 0; j < item.parts.length; j++) {
					parts.push(addStyle(item.parts[j], options));
				}
				stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
			}
		}
	}
	
	function listToStyles(list) {
		var styles = [];
		var newStyles = {};
		for(var i = 0; i < list.length; i++) {
			var item = list[i];
			var id = item[0];
			var css = item[1];
			var media = item[2];
			var sourceMap = item[3];
			var part = {css: css, media: media, sourceMap: sourceMap};
			if(!newStyles[id])
				styles.push(newStyles[id] = {id: id, parts: [part]});
			else
				newStyles[id].parts.push(part);
		}
		return styles;
	}
	
	function insertStyleElement(options, styleElement) {
		var head = getHeadElement();
		var lastStyleElementInsertedAtTop = styleElementsInsertedAtTop[styleElementsInsertedAtTop.length - 1];
		if (options.insertAt === "top") {
			if(!lastStyleElementInsertedAtTop) {
				head.insertBefore(styleElement, head.firstChild);
			} else if(lastStyleElementInsertedAtTop.nextSibling) {
				head.insertBefore(styleElement, lastStyleElementInsertedAtTop.nextSibling);
			} else {
				head.appendChild(styleElement);
			}
			styleElementsInsertedAtTop.push(styleElement);
		} else if (options.insertAt === "bottom") {
			head.appendChild(styleElement);
		} else {
			throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
		}
	}
	
	function removeStyleElement(styleElement) {
		styleElement.parentNode.removeChild(styleElement);
		var idx = styleElementsInsertedAtTop.indexOf(styleElement);
		if(idx >= 0) {
			styleElementsInsertedAtTop.splice(idx, 1);
		}
	}
	
	function createStyleElement(options) {
		var styleElement = document.createElement("style");
		styleElement.type = "text/css";
		insertStyleElement(options, styleElement);
		return styleElement;
	}
	
	function createLinkElement(options) {
		var linkElement = document.createElement("link");
		linkElement.rel = "stylesheet";
		insertStyleElement(options, linkElement);
		return linkElement;
	}
	
	function addStyle(obj, options) {
		var styleElement, update, remove;
	
		if (options.singleton) {
			var styleIndex = singletonCounter++;
			styleElement = singletonElement || (singletonElement = createStyleElement(options));
			update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
			remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
		} else if(obj.sourceMap &&
			typeof URL === "function" &&
			typeof URL.createObjectURL === "function" &&
			typeof URL.revokeObjectURL === "function" &&
			typeof Blob === "function" &&
			typeof btoa === "function") {
			styleElement = createLinkElement(options);
			update = updateLink.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
				if(styleElement.href)
					URL.revokeObjectURL(styleElement.href);
			};
		} else {
			styleElement = createStyleElement(options);
			update = applyToTag.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
			};
		}
	
		update(obj);
	
		return function updateStyle(newObj) {
			if(newObj) {
				if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
					return;
				update(obj = newObj);
			} else {
				remove();
			}
		};
	}
	
	var replaceText = (function () {
		var textStore = [];
	
		return function (index, replacement) {
			textStore[index] = replacement;
			return textStore.filter(Boolean).join('\n');
		};
	})();
	
	function applyToSingletonTag(styleElement, index, remove, obj) {
		var css = remove ? "" : obj.css;
	
		if (styleElement.styleSheet) {
			styleElement.styleSheet.cssText = replaceText(index, css);
		} else {
			var cssNode = document.createTextNode(css);
			var childNodes = styleElement.childNodes;
			if (childNodes[index]) styleElement.removeChild(childNodes[index]);
			if (childNodes.length) {
				styleElement.insertBefore(cssNode, childNodes[index]);
			} else {
				styleElement.appendChild(cssNode);
			}
		}
	}
	
	function applyToTag(styleElement, obj) {
		var css = obj.css;
		var media = obj.media;
	
		if(media) {
			styleElement.setAttribute("media", media)
		}
	
		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = css;
		} else {
			while(styleElement.firstChild) {
				styleElement.removeChild(styleElement.firstChild);
			}
			styleElement.appendChild(document.createTextNode(css));
		}
	}
	
	function updateLink(linkElement, obj) {
		var css = obj.css;
		var sourceMap = obj.sourceMap;
	
		if(sourceMap) {
			// http://stackoverflow.com/a/26603875
			css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
		}
	
		var blob = new Blob([css], { type: "text/css" });
	
		var oldSrc = linkElement.href;
	
		linkElement.href = URL.createObjectURL(blob);
	
		if(oldSrc)
			URL.revokeObjectURL(oldSrc);
	}


/***/ })
/******/ ]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgODhiM2RkNzE0NzQ2ZmIwY2MwZTQiLCJ3ZWJwYWNrOi8vLy4vbWFpbi5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvYXBwLmpzIiwid2VicGFjazovLy8uL3N0eWxlcy9tYWluLnNjc3M/ZmU3MiIsIndlYnBhY2s6Ly8vLi9zdHlsZXMvbWFpbi5zY3NzIiwid2VicGFjazovLy8uL34vY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanMiLCJ3ZWJwYWNrOi8vLy4vfi9zdHlsZS1sb2FkZXIvYWRkU3R5bGVzLmpzIl0sIm5hbWVzIjpbIkRCIiwiSlNPTiIsInBhcnNlIiwibG9jYWxTdG9yYWdlIiwiZ2V0SXRlbSIsImxlbmd0aCIsInRvZG8iLCJjbGVhcmVkIiwiaWQiLCJjb25zb2xlIiwibG9nIiwicmVxdWlyZSIsIiQiLCJrZXlwcmVzcyIsImV2ZW50Iiwid2hpY2giLCJ2YWwiLCJ0b2RvT2JqIiwicHVzaCIsInVwZGF0ZUxpc3QiLCJhbGVydCIsIndpbmRvdyIsImNsZWFyVG9nZ2xlIiwiZm9yRWFjaCIsImRlbGV0ZVRvZG8iLCJpbmRleCIsInNwbGljZSIsImZpbHRlciIsImZpbHRlclRvZG8iLCJtb2RlIiwiaXRlbXNMZWZ0IiwiaXRlbXMiLCJodG1sIiwiY2xlYXJlZENvdW50IiwiYWRkQ2xhc3MiLCJzZWxlY3Rpb25Nb2RlIiwicmVtb3ZlQ2xhc3MiLCJjbGVhckNvbXBsZXRlZCIsInNlbGVjdFRvZ2dsZSIsImljb24iLCJ0b2dnbGVDbGFzcyIsInRvZG9FZGl0YWJsZSIsImRibGNsaWNrIiwiZSIsImVsZW1lbnQiLCJjdXJyZW50VGFyZ2V0IiwicGFyc2VJbnQiLCJhdHRyIiwiaW5wdXQiLCJlbXB0eSIsImZvY3VzIiwiZm9jdXNvdXQiLCJmaWx0ZXJlZERCIiwidXBkYXRlZGxpc3QiLCJhcHBlbmQiLCJjbGljayIsIm9uYmVmb3JldW5sb2FkIiwic2V0SXRlbSIsInN0cmluZ2lmeSJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLHVCQUFlO0FBQ2Y7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7OztBQ3BDQTs7QUFFQSx3Qjs7Ozs7Ozs7QUNKQTtBQUNBLEtBQUlBLFdBQUo7QUFDQSxLQUFJQyxLQUFLQyxLQUFMLENBQVdDLGFBQWFDLE9BQWIsQ0FBcUIsSUFBckIsQ0FBWCxNQUEyQyxJQUEzQyxJQUNBSCxLQUFLQyxLQUFMLENBQVdDLGFBQWFDLE9BQWIsQ0FBcUIsSUFBckIsQ0FBWCxFQUF1Q0MsTUFBdkMsS0FBa0QsQ0FEdEQsRUFDeUQ7QUFDckRMLFVBQUssQ0FDRCxFQUFFTSxNQUFNLGdCQUFSLEVBQTBCQyxTQUFTLFNBQW5DLEVBQThDQyxJQUFJLENBQWxELEVBREMsRUFFRCxFQUFFRixNQUFNLEtBQVIsRUFBZUMsU0FBUyxFQUF4QixFQUE0QkMsSUFBSSxDQUFoQyxFQUZDLEVBR0QsRUFBRUYsTUFBTSxTQUFSLEVBQW1CQyxTQUFTLEVBQTVCLEVBQWdDQyxJQUFJLENBQXBDLEVBSEMsQ0FBTDtBQUtILEVBUEQsTUFPTztBQUNIUixVQUFLQyxLQUFLQyxLQUFMLENBQVdDLGFBQWFDLE9BQWIsQ0FBcUIsSUFBckIsQ0FBWCxDQUFMO0FBQ0g7QUFDREssU0FBUUMsR0FBUixDQUFZVCxLQUFLQyxLQUFMLENBQVdDLGFBQWFDLE9BQWIsQ0FBcUIsSUFBckIsQ0FBWCxDQUFaO0FBQ0E7QUFDQSxvQkFBQU8sQ0FBUSwrSUFBUjtBQUNBOztBQUVBQyxHQUFFLGFBQUYsRUFBaUJDLFFBQWpCLENBQTBCLFVBQUNDLEtBQUQsRUFBVztBQUNqQyxTQUFJQSxNQUFNQyxLQUFOLEtBQWdCLEVBQXBCLEVBQXdCO0FBQ3BCLGFBQUlILEVBQUUsYUFBRixFQUFpQkksR0FBakIsT0FBMkIsRUFBL0IsRUFBbUM7QUFDL0IsaUJBQUlWLE9BQU8sSUFBSVcsT0FBSixDQUFZTCxFQUFFLGFBQUYsRUFBaUJJLEdBQWpCLEVBQVosQ0FBWDtBQUNBaEIsZ0JBQUdrQixJQUFILENBQVFaLElBQVI7QUFDQUcscUJBQVFDLEdBQVIsQ0FBWVYsRUFBWjtBQUNBWSxlQUFFLGFBQUYsRUFBaUJJLEdBQWpCLENBQXFCLEVBQXJCO0FBQ0FHO0FBQ0gsVUFORCxNQU1PO0FBQ0hDLG1CQUFNLDRCQUFOO0FBQ0g7QUFFSjtBQUNKLEVBYkQ7O0FBZUFDLFFBQU9DLFdBQVAsR0FBcUIsVUFBU2QsRUFBVCxFQUFhO0FBQzlCO0FBQ0FSLFFBQUd1QixPQUFILENBQVcsVUFBQ04sT0FBRCxFQUFhO0FBQ3BCLGFBQUlBLFFBQVFULEVBQVIsS0FBZUEsRUFBbkIsRUFBdUI7QUFDbkIsaUJBQUlTLFFBQVFWLE9BQVIsS0FBb0IsRUFBeEIsRUFBNEI7QUFDeEJVLHlCQUFRVixPQUFSLEdBQWtCLFNBQWxCO0FBQ0gsY0FGRCxNQUVPO0FBQ0hVLHlCQUFRVixPQUFSLEdBQWtCLEVBQWxCO0FBQ0g7QUFDSjtBQUNKLE1BUkQ7QUFTQVk7QUFDSCxFQVpEOztBQWNBRSxRQUFPRyxVQUFQLEdBQW9CLFVBQVNoQixFQUFULEVBQWE7QUFDN0I7QUFDQVIsUUFBR3VCLE9BQUgsQ0FBVyxVQUFDTixPQUFELEVBQVVRLEtBQVYsRUFBb0I7QUFDM0IsYUFBSVIsUUFBUVQsRUFBUixLQUFlQSxFQUFuQixFQUF1QjtBQUNuQlIsZ0JBQUcwQixNQUFILENBQVVELEtBQVYsRUFBaUIsQ0FBakI7QUFDSDtBQUNKLE1BSkQ7QUFLQU47QUFDSCxFQVJEOztBQVVBLEtBQUlRLGVBQUo7O0FBRUEsVUFBU0MsVUFBVCxDQUFvQkMsSUFBcEIsRUFBMEI7QUFDdEJGLGNBQVNFLElBQVQ7QUFDQVY7QUFDSDs7QUFFRCxVQUFTVyxTQUFULEdBQXFCO0FBQ2pCLFNBQUlDLFFBQVEvQixHQUFHMkIsTUFBSCxDQUFVLFVBQUNWLE9BQUQsRUFBYTtBQUMvQixnQkFBT0EsUUFBUVYsT0FBUixLQUFvQixFQUEzQjtBQUNILE1BRlcsQ0FBWjtBQUdBSyxPQUFFLG9CQUFGLEVBQXdCb0IsSUFBeEIsQ0FBNkJELE1BQU0xQixNQUFuQztBQUNIOztBQUVELFVBQVM0QixZQUFULEdBQXdCO0FBQ3BCLFNBQUlGLFFBQVEvQixHQUFHMkIsTUFBSCxDQUFVLFVBQUNWLE9BQUQsRUFBYTtBQUMvQixnQkFBT0EsUUFBUVYsT0FBUixLQUFvQixTQUEzQjtBQUNILE1BRlcsQ0FBWjtBQUdBSyxPQUFFLHVCQUFGLEVBQTJCb0IsSUFBM0IsQ0FBZ0NELE1BQU0xQixNQUF0QztBQUNBLFNBQUkwQixNQUFNMUIsTUFBTixLQUFpQkwsR0FBR0ssTUFBeEIsRUFBZ0M7QUFDNUJPLFdBQUUsa0JBQUYsRUFBc0JzQixRQUF0QixDQUErQixPQUEvQjtBQUNBQyx5QkFBZ0IsVUFBaEI7QUFDSCxNQUhELE1BR087QUFDSHZCLFdBQUUsa0JBQUYsRUFBc0J3QixXQUF0QixDQUFrQyxPQUFsQztBQUNBRCx5QkFBZ0IsUUFBaEI7QUFDSDtBQUNKOztBQUVELFVBQVNFLGNBQVQsR0FBMEI7QUFDdEJyQyxVQUFLQSxHQUFHMkIsTUFBSCxDQUFVLFVBQUNWLE9BQUQsRUFBYTtBQUN4QixnQkFBT0EsUUFBUVYsT0FBUixLQUFvQixFQUEzQjtBQUNILE1BRkksQ0FBTDtBQUdBWTtBQUNIOztBQUVELEtBQUlnQixnQkFBZ0IsUUFBcEI7O0FBRUEsVUFBU0csWUFBVCxHQUF3QjtBQUNwQixTQUFJQyxPQUFPM0IsRUFBRSxrQkFBRixDQUFYO0FBQ0EsU0FBSXVCLGtCQUFrQixRQUF0QixFQUFnQztBQUM1Qm5DLFlBQUd1QixPQUFILENBQVcsVUFBQ04sT0FBRCxFQUFhO0FBQ3BCQSxxQkFBUVYsT0FBUixHQUFrQixTQUFsQjtBQUNILFVBRkQ7QUFHQWdDLGNBQUtDLFdBQUwsQ0FBaUIsT0FBakI7QUFDQUwseUJBQWdCLFVBQWhCO0FBQ0gsTUFORCxNQU1PO0FBQ0huQyxZQUFHdUIsT0FBSCxDQUFXLFVBQUNOLE9BQUQsRUFBYTtBQUNwQkEscUJBQVFWLE9BQVIsR0FBa0IsRUFBbEI7QUFDSCxVQUZEO0FBR0E0Qix5QkFBZ0IsUUFBaEI7QUFDQUksY0FBS0MsV0FBTCxDQUFpQixPQUFqQjtBQUNIO0FBQ0RyQjtBQUVIOztBQUVELFVBQVNzQixZQUFULEdBQXdCO0FBQ3BCN0IsT0FBRSxJQUFGLEVBQVE4QixRQUFSLENBQWlCLFVBQUNDLENBQUQsRUFBTztBQUNwQixhQUFJQyxVQUFVaEMsRUFBRStCLEVBQUVFLGFBQUosQ0FBZCxDQURvQixDQUNjO0FBQ2xDLGFBQUlyQyxLQUFLc0MsU0FBU0YsUUFBUUcsSUFBUixDQUFhLFNBQWIsQ0FBVCxDQUFUO0FBQ0EsYUFBSUMsUUFBUSxFQUFaO0FBQ0FoRCxZQUFHdUIsT0FBSCxDQUFXLFVBQUNOLE9BQUQsRUFBYTtBQUNwQixpQkFBSUEsUUFBUVQsRUFBUixLQUFlQSxFQUFuQixFQUF1QjtBQUNuQndDLDJGQUNxQnhDLEVBRHJCLGtEQUNvRVMsUUFBUVgsSUFENUU7QUFHSDtBQUNKLFVBTkQ7QUFPQXNDLGlCQUFRSyxLQUFSLEdBQWdCakIsSUFBaEIsQ0FBcUJnQixLQUFyQjtBQUNBcEMsV0FBRSxVQUFVSixFQUFaLEVBQWdCMEMsS0FBaEI7QUFDQXRDLFdBQUUsVUFBVUosRUFBWixFQUFnQkssUUFBaEIsQ0FBeUIsWUFBTTtBQUMzQixpQkFBSUMsTUFBTUMsS0FBTixLQUFnQixFQUFwQixFQUF3QjtBQUNwQmYsb0JBQUd1QixPQUFILENBQVcsVUFBQ04sT0FBRCxFQUFhO0FBQ3BCLHlCQUFJQSxRQUFRVCxFQUFSLEtBQWVBLEVBQW5CLEVBQXVCO0FBQ25CUyxpQ0FBUVgsSUFBUixHQUFlTSxFQUFFLFVBQVVKLEVBQVosRUFBZ0JRLEdBQWhCLEVBQWY7QUFDSDtBQUNKLGtCQUpEO0FBS0FHO0FBQ0g7QUFDSixVQVREO0FBVUFQLFdBQUUsVUFBVUosRUFBWixFQUFnQjJDLFFBQWhCLENBQXlCLFlBQU07QUFDM0JuRCxnQkFBR3VCLE9BQUgsQ0FBVyxVQUFDTixPQUFELEVBQWE7QUFDcEIscUJBQUlBLFFBQVFULEVBQVIsS0FBZUEsRUFBbkIsRUFBdUI7QUFDbkJTLDZCQUFRWCxJQUFSLEdBQWVNLEVBQUUsVUFBVUosRUFBWixFQUFnQlEsR0FBaEIsRUFBZjtBQUNIO0FBQ0osY0FKRDtBQUtBRztBQUNILFVBUEQ7QUFRSCxNQS9CRDtBQWdDSDs7QUFFRCxVQUFTQSxVQUFULEdBQXNCO0FBQ2xCLFNBQUlpQyxtQkFBSjtBQUNBLFNBQUlDLGNBQWMsRUFBbEI7QUFDQXpDLE9BQUUsWUFBRixFQUFnQnFDLEtBQWhCO0FBQ0EsU0FBSXRCLFdBQVcsUUFBZixFQUF5QjtBQUNyQnlCLHNCQUFhcEQsR0FBRzJCLE1BQUgsQ0FBVSxVQUFDVixPQUFELEVBQWE7QUFDaEMsb0JBQU9BLFFBQVFWLE9BQVIsS0FBb0IsRUFBM0I7QUFDSCxVQUZZLENBQWI7QUFHSCxNQUpELE1BSU8sSUFBSW9CLFdBQVcsV0FBZixFQUE0QjtBQUMvQnlCLHNCQUFhcEQsR0FBRzJCLE1BQUgsQ0FBVSxVQUFDVixPQUFELEVBQWE7QUFDaEMsb0JBQU9BLFFBQVFWLE9BQVIsS0FBb0IsU0FBM0I7QUFDSCxVQUZZLENBQWI7QUFHSCxNQUpNLE1BSUE7QUFDSDZDLHNCQUFhcEQsRUFBYjtBQUNIO0FBQ0RvRCxnQkFBVzdCLE9BQVgsQ0FBbUIsVUFBQ04sT0FBRCxFQUFhO0FBQzVCb0Msa0VBQzZCcEMsUUFBUVYsT0FEckMsbUJBQzBEVSxRQUFRVCxFQURsRSx1R0FFcUZTLFFBQVFULEVBRjdGLDJHQUtNUyxRQUFRWCxJQUxkLCtGQU0rRVcsUUFBUVQsRUFOdkY7QUFVSCxNQVhEO0FBWUFJLE9BQUUsWUFBRixFQUFnQjBDLE1BQWhCLENBQXVCRCxXQUF2QjtBQUNBdkI7QUFDQUc7QUFDQVE7QUFDSDs7QUFFRDs7QUFFQTdCLEdBQUUsYUFBRixFQUFpQjJDLEtBQWpCLENBQXVCLFlBQU07QUFDekIzQixnQkFBVyxLQUFYO0FBQ0gsRUFGRDs7QUFJQWhCLEdBQUUsZ0JBQUYsRUFBb0IyQyxLQUFwQixDQUEwQixZQUFNO0FBQzVCM0IsZ0JBQVcsUUFBWDtBQUNILEVBRkQ7O0FBSUFoQixHQUFFLG1CQUFGLEVBQXVCMkMsS0FBdkIsQ0FBNkIsWUFBTTtBQUMvQjNCLGdCQUFXLFdBQVg7QUFDSCxFQUZEOztBQUlBaEIsR0FBRSxrQkFBRixFQUFzQjJDLEtBQXRCLENBQTRCLFlBQU07QUFDOUJsQjtBQUNILEVBRkQ7O0FBSUF6QixHQUFFLGdCQUFGLEVBQW9CMkMsS0FBcEIsQ0FBMEIsWUFBTTtBQUM1QmpCO0FBQ0gsRUFGRDtBQUdBbkI7O0FBS0FFLFFBQU9tQyxjQUFQLEdBQXdCLFlBQVc7QUFDL0JyRCxrQkFBYXNELE9BQWIsQ0FBcUIsSUFBckIsRUFBMkJ4RCxLQUFLeUQsU0FBTCxDQUFlMUQsRUFBZixDQUEzQjtBQUNILEVBRkQsQzs7Ozs7O0FDOU1BOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0RBQThFO0FBQzlFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUc7QUFDSDtBQUNBO0FBQ0EsaUNBQWdDLFVBQVUsRUFBRTtBQUM1QyxFOzs7Ozs7QUNwQkE7QUFDQTs7O0FBR0E7QUFDQSxzQ0FBcUMsMEJBQTBCLDBCQUEwQixnQkFBZ0IsdUJBQXVCLG9CQUFvQixFQUFFLG1CQUFtQix1QkFBdUIsd0JBQXdCLGlDQUFpQyxFQUFFLGFBQWEsdUJBQXVCLGVBQWUsRUFBRSxzQkFBc0Isd0JBQXdCLEVBQUUsT0FBTyxjQUFjLGlCQUFpQixFQUFFLGdCQUFnQixpQkFBaUIsRUFBRSxjQUFjLGtDQUFrQyxFQUFFLDRCQUE0QixtQkFBbUIsRUFBRSxzQkFBc0IscUJBQXFCLEVBQUUsWUFBWSxpQkFBaUIsRUFBRSxXQUFXLGtCQUFrQixtQ0FBbUMsRUFBRTs7QUFFNXBCOzs7Ozs7O0FDUEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsaUJBQWdCLGlCQUFpQjtBQUNqQztBQUNBO0FBQ0EseUNBQXdDLGdCQUFnQjtBQUN4RCxLQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWdCLGlCQUFpQjtBQUNqQztBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQVksb0JBQW9CO0FBQ2hDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQ2pEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFvQjtBQUNwQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFFO0FBQ0Y7QUFDQTtBQUNBLEdBQUU7QUFDRjtBQUNBO0FBQ0EsR0FBRTtBQUNGO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGlCQUFnQixtQkFBbUI7QUFDbkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWdCLHNCQUFzQjtBQUN0QztBQUNBO0FBQ0EsbUJBQWtCLDJCQUEyQjtBQUM3QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxnQkFBZSxtQkFBbUI7QUFDbEM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBaUIsMkJBQTJCO0FBQzVDO0FBQ0E7QUFDQSxTQUFRLHVCQUF1QjtBQUMvQjtBQUNBO0FBQ0EsSUFBRztBQUNIO0FBQ0Esa0JBQWlCLHVCQUF1QjtBQUN4QztBQUNBO0FBQ0EsNEJBQTJCO0FBQzNCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnQkFBZSxpQkFBaUI7QUFDaEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWM7QUFDZDtBQUNBLGlDQUFnQyxzQkFBc0I7QUFDdEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFHO0FBQ0g7QUFDQSxJQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRTtBQUNGO0FBQ0EsR0FBRTtBQUNGO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUM7O0FBRUQ7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLHdEQUF1RDtBQUN2RDs7QUFFQSw4QkFBNkIsbUJBQW1COztBQUVoRDs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EiLCJmaWxlIjoiYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pXG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG5cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGV4cG9ydHM6IHt9LFxuIFx0XHRcdGlkOiBtb2R1bGVJZCxcbiBcdFx0XHRsb2FkZWQ6IGZhbHNlXG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmxvYWRlZCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oMCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgODhiM2RkNzE0NzQ2ZmIwY2MwZTQiLCIvL0FsbCBzY3JpcHRzIHN0YXJ0IGZyb20gc3JjL2luZGV4LmpzXG4vL1BsZWFzZSBkbyBub3QgYWRkIGFueXRoaW5nIGhlcmVcbmltcG9ydCAnLi9zcmMvYXBwJztcblxuaW1wb3J0ICcuL3N0eWxlcy9tYWluLnNjc3MnO1xuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL21haW4uanMiLCIvL0RhdGFCYXNlO1xubGV0IERCO1xuaWYgKEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ0RCJykpID09PSBudWxsIHx8XG4gICAgSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnREInKSkubGVuZ3RoID09PSAwKSB7XG4gICAgREIgPSBbXG4gICAgICAgIHsgdG9kbzogJ0xlYXJuIEVtYmVyLmpzJywgY2xlYXJlZDogJ2NsZWFyZWQnLCBpZDogMCB9LFxuICAgICAgICB7IHRvZG86ICcuLi4nLCBjbGVhcmVkOiAnJywgaWQ6IDEgfSxcbiAgICAgICAgeyB0b2RvOiAnUHJvZml0IScsIGNsZWFyZWQ6ICcnLCBpZDogMiB9XG4gICAgXTtcbn0gZWxzZSB7XG4gICAgREIgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdEQicpKTtcbn1cbmNvbnNvbGUubG9nKEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ0RCJykpKTtcbi8vT2JqZWN0IENvbnN0cnVjdG9yXG5yZXF1aXJlKCcuL3NyYy9tb2RlbC90b2RvT2JqJyk7XG4vL0NvbnRyb2xsZXJzXG5cbiQoJyN0b2RvLWlucHV0Jykua2V5cHJlc3MoKGV2ZW50KSA9PiB7XG4gICAgaWYgKGV2ZW50LndoaWNoID09PSAxMykge1xuICAgICAgICBpZiAoJCgnI3RvZG8taW5wdXQnKS52YWwoKSAhPT0gJycpIHtcbiAgICAgICAgICAgIGxldCB0b2RvID0gbmV3IHRvZG9PYmooJChcIiN0b2RvLWlucHV0XCIpLnZhbCgpKTtcbiAgICAgICAgICAgIERCLnB1c2godG9kbyk7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhEQik7XG4gICAgICAgICAgICAkKFwiI3RvZG8taW5wdXRcIikudmFsKCcnKTtcbiAgICAgICAgICAgIHVwZGF0ZUxpc3QoKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGFsZXJ0KCdZb3UgZGlkblxcJ3QgZW50ZXIgYW55dGhpbmcnKTtcbiAgICAgICAgfVxuXG4gICAgfVxufSk7XG5cbndpbmRvdy5jbGVhclRvZ2dsZSA9IGZ1bmN0aW9uKGlkKSB7XG4gICAgLy9XaW5kb3cgdXNlZCB0byBleHBvc2UgZnVuY3Rpb25zIGdsb2JhbGx5IGZvciB1c2UgaW5zaWRlIGh0bWxcbiAgICBEQi5mb3JFYWNoKCh0b2RvT2JqKSA9PiB7XG4gICAgICAgIGlmICh0b2RvT2JqLmlkID09PSBpZCkge1xuICAgICAgICAgICAgaWYgKHRvZG9PYmouY2xlYXJlZCA9PT0gJycpIHtcbiAgICAgICAgICAgICAgICB0b2RvT2JqLmNsZWFyZWQgPSAnY2xlYXJlZCc7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHRvZG9PYmouY2xlYXJlZCA9ICcnO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSk7XG4gICAgdXBkYXRlTGlzdCgpO1xufVxuXG53aW5kb3cuZGVsZXRlVG9kbyA9IGZ1bmN0aW9uKGlkKSB7XG4gICAgLy9XaW5kb3cgdXNlZCB0byBleHBvc2UgZnVuY3Rpb25zIGdsb2JhbGx5IGZvciB1c2UgaW5zaWRlIGh0bWxcbiAgICBEQi5mb3JFYWNoKCh0b2RvT2JqLCBpbmRleCkgPT4ge1xuICAgICAgICBpZiAodG9kb09iai5pZCA9PT0gaWQpIHtcbiAgICAgICAgICAgIERCLnNwbGljZShpbmRleCwgMSk7XG4gICAgICAgIH1cbiAgICB9KTtcbiAgICB1cGRhdGVMaXN0KCk7XG59XG5cbmxldCBmaWx0ZXJcblxuZnVuY3Rpb24gZmlsdGVyVG9kbyhtb2RlKSB7XG4gICAgZmlsdGVyID0gbW9kZTtcbiAgICB1cGRhdGVMaXN0KCk7XG59XG5cbmZ1bmN0aW9uIGl0ZW1zTGVmdCgpIHtcbiAgICBsZXQgaXRlbXMgPSBEQi5maWx0ZXIoKHRvZG9PYmopID0+IHtcbiAgICAgICAgcmV0dXJuIHRvZG9PYmouY2xlYXJlZCA9PT0gJyc7XG4gICAgfSk7XG4gICAgJCgnI2l0ZW1zLWxlZnQgLmJhZGdlJykuaHRtbChpdGVtcy5sZW5ndGgpO1xufVxuXG5mdW5jdGlvbiBjbGVhcmVkQ291bnQoKSB7XG4gICAgbGV0IGl0ZW1zID0gREIuZmlsdGVyKCh0b2RvT2JqKSA9PiB7XG4gICAgICAgIHJldHVybiB0b2RvT2JqLmNsZWFyZWQgPT09ICdjbGVhcmVkJztcbiAgICB9KTtcbiAgICAkKCcjaXRlbXMtY2xlYXJlZCAuYmFkZ2UnKS5odG1sKGl0ZW1zLmxlbmd0aCk7XG4gICAgaWYgKGl0ZW1zLmxlbmd0aCA9PT0gREIubGVuZ3RoKSB7XG4gICAgICAgICQoJyNzZWxlY3QtYWxsLWljb24nKS5hZGRDbGFzcygnZ3JlZW4nKTtcbiAgICAgICAgc2VsZWN0aW9uTW9kZSA9ICd1bnNlbGVjdCc7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgJCgnI3NlbGVjdC1hbGwtaWNvbicpLnJlbW92ZUNsYXNzKCdncmVlbicpO1xuICAgICAgICBzZWxlY3Rpb25Nb2RlID0gJ3NlbGVjdCc7XG4gICAgfVxufVxuXG5mdW5jdGlvbiBjbGVhckNvbXBsZXRlZCgpIHtcbiAgICBEQiA9IERCLmZpbHRlcigodG9kb09iaikgPT4ge1xuICAgICAgICByZXR1cm4gdG9kb09iai5jbGVhcmVkID09PSAnJztcbiAgICB9KTtcbiAgICB1cGRhdGVMaXN0KCk7XG59XG5cbmxldCBzZWxlY3Rpb25Nb2RlID0gJ3NlbGVjdCc7XG5cbmZ1bmN0aW9uIHNlbGVjdFRvZ2dsZSgpIHtcbiAgICBsZXQgaWNvbiA9ICQoJyNzZWxlY3QtYWxsLWljb24nKTtcbiAgICBpZiAoc2VsZWN0aW9uTW9kZSA9PT0gJ3NlbGVjdCcpIHtcbiAgICAgICAgREIuZm9yRWFjaCgodG9kb09iaikgPT4ge1xuICAgICAgICAgICAgdG9kb09iai5jbGVhcmVkID0gJ2NsZWFyZWQnO1xuICAgICAgICB9KTtcbiAgICAgICAgaWNvbi50b2dnbGVDbGFzcyhcImdyZWVuXCIpO1xuICAgICAgICBzZWxlY3Rpb25Nb2RlID0gJ3Vuc2VsZWN0JztcbiAgICB9IGVsc2Uge1xuICAgICAgICBEQi5mb3JFYWNoKCh0b2RvT2JqKSA9PiB7XG4gICAgICAgICAgICB0b2RvT2JqLmNsZWFyZWQgPSAnJztcbiAgICAgICAgfSk7XG4gICAgICAgIHNlbGVjdGlvbk1vZGUgPSAnc2VsZWN0JztcbiAgICAgICAgaWNvbi50b2dnbGVDbGFzcyhcImdyZWVuXCIpO1xuICAgIH1cbiAgICB1cGRhdGVMaXN0KCk7XG5cbn1cblxuZnVuY3Rpb24gdG9kb0VkaXRhYmxlKCkge1xuICAgICQoJ2xpJykuZGJsY2xpY2soKGUpID0+IHtcbiAgICAgICAgbGV0IGVsZW1lbnQgPSAkKGUuY3VycmVudFRhcmdldCk7IC8vJCh0aGlzKSB3YXMgcmV0dXJuaW5nIHRoZSB3aG9sZSBkb2N1bWVudCBvYmplY3Qgd2VpcmRseSwgQSB3b3JrYXJvdW5kIHVzZWQuXG4gICAgICAgIGxldCBpZCA9IHBhcnNlSW50KGVsZW1lbnQuYXR0cignZGF0YS1pZCcpKTtcbiAgICAgICAgbGV0IGlucHV0ID0gJyc7XG4gICAgICAgIERCLmZvckVhY2goKHRvZG9PYmopID0+IHtcbiAgICAgICAgICAgIGlmICh0b2RvT2JqLmlkID09PSBpZCkge1xuICAgICAgICAgICAgICAgIGlucHV0ID0gYDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG4gICAgICAgICAgICAgICAgICAgIDxpbnB1dCBpZD1cImVkaXQke2lkfVwiIHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiB2YWx1ZT1cIiR7dG9kb09iai50b2RvfVwiPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5gO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgICAgZWxlbWVudC5lbXB0eSgpLmh0bWwoaW5wdXQpO1xuICAgICAgICAkKCcjZWRpdCcgKyBpZCkuZm9jdXMoKTtcbiAgICAgICAgJCgnI2VkaXQnICsgaWQpLmtleXByZXNzKCgpID0+IHtcbiAgICAgICAgICAgIGlmIChldmVudC53aGljaCA9PT0gMTMpIHtcbiAgICAgICAgICAgICAgICBEQi5mb3JFYWNoKCh0b2RvT2JqKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGlmICh0b2RvT2JqLmlkID09PSBpZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdG9kb09iai50b2RvID0gJCgnI2VkaXQnICsgaWQpLnZhbCgpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgdXBkYXRlTGlzdCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgICAgJCgnI2VkaXQnICsgaWQpLmZvY3Vzb3V0KCgpID0+IHtcbiAgICAgICAgICAgIERCLmZvckVhY2goKHRvZG9PYmopID0+IHtcbiAgICAgICAgICAgICAgICBpZiAodG9kb09iai5pZCA9PT0gaWQpIHtcbiAgICAgICAgICAgICAgICAgICAgdG9kb09iai50b2RvID0gJCgnI2VkaXQnICsgaWQpLnZhbCgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgdXBkYXRlTGlzdCgpO1xuICAgICAgICB9KTtcbiAgICB9KTtcbn1cblxuZnVuY3Rpb24gdXBkYXRlTGlzdCgpIHtcbiAgICBsZXQgZmlsdGVyZWREQjtcbiAgICBsZXQgdXBkYXRlZGxpc3QgPSAnJztcbiAgICAkKCcjdG9kby1saXN0JykuZW1wdHkoKTtcbiAgICBpZiAoZmlsdGVyID09PSAnYWN0aXZlJykge1xuICAgICAgICBmaWx0ZXJlZERCID0gREIuZmlsdGVyKCh0b2RvT2JqKSA9PiB7XG4gICAgICAgICAgICByZXR1cm4gdG9kb09iai5jbGVhcmVkID09PSAnJztcbiAgICAgICAgfSk7XG4gICAgfSBlbHNlIGlmIChmaWx0ZXIgPT09ICdjb21wbGV0ZWQnKSB7XG4gICAgICAgIGZpbHRlcmVkREIgPSBEQi5maWx0ZXIoKHRvZG9PYmopID0+IHtcbiAgICAgICAgICAgIHJldHVybiB0b2RvT2JqLmNsZWFyZWQgPT09ICdjbGVhcmVkJztcbiAgICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgZmlsdGVyZWREQiA9IERCO1xuICAgIH1cbiAgICBmaWx0ZXJlZERCLmZvckVhY2goKHRvZG9PYmopID0+IHtcbiAgICAgICAgdXBkYXRlZGxpc3QgKz0gYFxuICAgICAgICA8bGkgY2xhc3M9XCJsaXN0LWdyb3VwLWl0ZW0gJHt0b2RvT2JqLmNsZWFyZWR9XCIgZGF0YS1pZD1cIiR7dG9kb09iai5pZH1cIj5cbiAgICAgICAgICAgIDxhIGhyZWY9XCIjIVwiIGlkPVwiY2xlYXItdG9nZ2xlXCIgY2xhc3M9XCJ0b2RvLWJ0biBwdWxsLWxlZnRcIiBvbmNsaWNrPVwiY2xlYXJUb2dnbGUoJHt0b2RvT2JqLmlkfSlcIj5cbiAgICAgICAgICAgICAgICA8aSBjbGFzcz1cImZhIGZhLWNoZWNrXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPlxuICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgJHt0b2RvT2JqLnRvZG99XG4gICAgICAgICAgICA8YSBocmVmPVwiIyFcIiBpZD1cImRlbGV0ZVwiIGNsYXNzPVwidG9kby1idG4gcHVsbC1yaWdodFwiIG9uY2xpY2s9XCJkZWxldGVUb2RvKCR7dG9kb09iai5pZH0pXCI+XG4gICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJmYSBmYS10aW1lc1wiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT5cbiAgICAgICAgICAgIDwvYT5cbiAgICAgICAgPC9saT5gO1xuICAgIH0pO1xuICAgICQoJyN0b2RvLWxpc3QnKS5hcHBlbmQodXBkYXRlZGxpc3QpO1xuICAgIGl0ZW1zTGVmdCgpO1xuICAgIGNsZWFyZWRDb3VudCgpO1xuICAgIHRvZG9FZGl0YWJsZSgpO1xufVxuXG4vL0JpbmRpbmcgZnVuY3Rpb25zIHRvIGJ1dHRvbiBjbGljayBldmVudHNcblxuJCgnI2ZpbHRlci1hbGwnKS5jbGljaygoKSA9PiB7XG4gICAgZmlsdGVyVG9kbygnYWxsJyk7XG59KTtcblxuJCgnI2ZpbHRlci1hY3RpdmUnKS5jbGljaygoKSA9PiB7XG4gICAgZmlsdGVyVG9kbygnYWN0aXZlJyk7XG59KTtcblxuJCgnI2ZpbHRlci1jb21wbGV0ZWQnKS5jbGljaygoKSA9PiB7XG4gICAgZmlsdGVyVG9kbygnY29tcGxldGVkJyk7XG59KTtcblxuJCgnI2NsZWFyLWNvbXBsZXRlZCcpLmNsaWNrKCgpID0+IHtcbiAgICBjbGVhckNvbXBsZXRlZCgpO1xufSk7XG5cbiQoJyNzZWxlY3QtdG9nZ2xlJykuY2xpY2soKCkgPT4ge1xuICAgIHNlbGVjdFRvZ2dsZSgpO1xufSk7XG51cGRhdGVMaXN0KCk7XG5cblxuXG5cbndpbmRvdy5vbmJlZm9yZXVubG9hZCA9IGZ1bmN0aW9uKCkge1xuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdEQicsIEpTT04uc3RyaW5naWZ5KERCKSk7XG59XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2FwcC5qcyIsIi8vIHN0eWxlLWxvYWRlcjogQWRkcyBzb21lIGNzcyB0byB0aGUgRE9NIGJ5IGFkZGluZyBhIDxzdHlsZT4gdGFnXG5cbi8vIGxvYWQgdGhlIHN0eWxlc1xudmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcyEuLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvaW5kZXguanMhIS4vbWFpbi5zY3NzXCIpO1xuaWYodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4vLyBhZGQgdGhlIHN0eWxlcyB0byB0aGUgRE9NXG52YXIgdXBkYXRlID0gcmVxdWlyZShcIiEuLi9ub2RlX21vZHVsZXMvc3R5bGUtbG9hZGVyL2FkZFN0eWxlcy5qc1wiKShjb250ZW50LCB7fSk7XG5pZihjb250ZW50LmxvY2FscykgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2Fscztcbi8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbmlmKG1vZHVsZS5ob3QpIHtcblx0Ly8gV2hlbiB0aGUgc3R5bGVzIGNoYW5nZSwgdXBkYXRlIHRoZSA8c3R5bGU+IHRhZ3Ncblx0aWYoIWNvbnRlbnQubG9jYWxzKSB7XG5cdFx0bW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzIS4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9pbmRleC5qcyEhLi9tYWluLnNjc3NcIiwgZnVuY3Rpb24oKSB7XG5cdFx0XHR2YXIgbmV3Q29udGVudCA9IHJlcXVpcmUoXCIhIS4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzIS4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9pbmRleC5qcyEhLi9tYWluLnNjc3NcIik7XG5cdFx0XHRpZih0eXBlb2YgbmV3Q29udGVudCA9PT0gJ3N0cmluZycpIG5ld0NvbnRlbnQgPSBbW21vZHVsZS5pZCwgbmV3Q29udGVudCwgJyddXTtcblx0XHRcdHVwZGF0ZShuZXdDb250ZW50KTtcblx0XHR9KTtcblx0fVxuXHQvLyBXaGVuIHRoZSBtb2R1bGUgaXMgZGlzcG9zZWQsIHJlbW92ZSB0aGUgPHN0eWxlPiB0YWdzXG5cdG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgdXBkYXRlKCk7IH0pO1xufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc3R5bGVzL21haW4uc2Nzc1xuLy8gbW9kdWxlIGlkID0gMlxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKCk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCIudG9kby1idG4ge1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xcbiAgY29sb3I6IGdyZXk7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBmb250LXNpemU6IDE1cHg7IH1cXG5cXG4jY2xlYXItdG9nZ2xlIHtcXG4gIG1hcmdpbi1yaWdodDogMTBweDtcXG4gIHBhZGRpbmctcmlnaHQ6IDEwcHg7XFxuICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCBncmV5OyB9XFxuXFxuI2RlbGV0ZSB7XFxuICB2aXNpYmlsaXR5OiBoaWRkZW47XFxuICBjb2xvcjogcmVkOyB9XFxuXFxubGk6aG92ZXIgI2RlbGV0ZSB7XFxuICB2aXNpYmlsaXR5OiB2aXNpYmxlOyB9XFxuXFxucCB7XFxuICBtYXJnaW46IDA7XFxuICBwYWRkaW5nOiA0cHg7IH1cXG5cXG4ubWluaS13ZWxsIHtcXG4gIHBhZGRpbmc6IDRweDsgfVxcblxcbi5jbGVhcmVkIHtcXG4gIHRleHQtZGVjb3JhdGlvbjogbGluZS10aHJvdWdoOyB9XFxuICAuY2xlYXJlZCAjY2xlYXItdG9nZ2xlIHtcXG4gICAgY29sb3I6IGdyZWVuOyB9XFxuXFxuI3NlbGVjdC1hbGwtaWNvbiB7XFxuICBmb250LXdlaWdodDogOTAwOyB9XFxuXFxuLmdyZWVuIHtcXG4gIGNvbG9yOiBncmVlbjsgfVxcblxcbi5mbGV4IHtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47IH1cXG5cIiwgXCJcIl0pO1xuXG4vLyBleHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vY3NzLWxvYWRlciEuL34vc2Fzcy1sb2FkZXIhLi9zdHlsZXMvbWFpbi5zY3NzXG4vLyBtb2R1bGUgaWQgPSAzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsIi8qXHJcblx0TUlUIExpY2Vuc2UgaHR0cDovL3d3dy5vcGVuc291cmNlLm9yZy9saWNlbnNlcy9taXQtbGljZW5zZS5waHBcclxuXHRBdXRob3IgVG9iaWFzIEtvcHBlcnMgQHNva3JhXHJcbiovXHJcbi8vIGNzcyBiYXNlIGNvZGUsIGluamVjdGVkIGJ5IHRoZSBjc3MtbG9hZGVyXHJcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24oKSB7XHJcblx0dmFyIGxpc3QgPSBbXTtcclxuXHJcblx0Ly8gcmV0dXJuIHRoZSBsaXN0IG9mIG1vZHVsZXMgYXMgY3NzIHN0cmluZ1xyXG5cdGxpc3QudG9TdHJpbmcgPSBmdW5jdGlvbiB0b1N0cmluZygpIHtcclxuXHRcdHZhciByZXN1bHQgPSBbXTtcclxuXHRcdGZvcih2YXIgaSA9IDA7IGkgPCB0aGlzLmxlbmd0aDsgaSsrKSB7XHJcblx0XHRcdHZhciBpdGVtID0gdGhpc1tpXTtcclxuXHRcdFx0aWYoaXRlbVsyXSkge1xyXG5cdFx0XHRcdHJlc3VsdC5wdXNoKFwiQG1lZGlhIFwiICsgaXRlbVsyXSArIFwie1wiICsgaXRlbVsxXSArIFwifVwiKTtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRyZXN1bHQucHVzaChpdGVtWzFdKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0cmV0dXJuIHJlc3VsdC5qb2luKFwiXCIpO1xyXG5cdH07XHJcblxyXG5cdC8vIGltcG9ydCBhIGxpc3Qgb2YgbW9kdWxlcyBpbnRvIHRoZSBsaXN0XHJcblx0bGlzdC5pID0gZnVuY3Rpb24obW9kdWxlcywgbWVkaWFRdWVyeSkge1xyXG5cdFx0aWYodHlwZW9mIG1vZHVsZXMgPT09IFwic3RyaW5nXCIpXHJcblx0XHRcdG1vZHVsZXMgPSBbW251bGwsIG1vZHVsZXMsIFwiXCJdXTtcclxuXHRcdHZhciBhbHJlYWR5SW1wb3J0ZWRNb2R1bGVzID0ge307XHJcblx0XHRmb3IodmFyIGkgPSAwOyBpIDwgdGhpcy5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHR2YXIgaWQgPSB0aGlzW2ldWzBdO1xyXG5cdFx0XHRpZih0eXBlb2YgaWQgPT09IFwibnVtYmVyXCIpXHJcblx0XHRcdFx0YWxyZWFkeUltcG9ydGVkTW9kdWxlc1tpZF0gPSB0cnVlO1xyXG5cdFx0fVxyXG5cdFx0Zm9yKGkgPSAwOyBpIDwgbW9kdWxlcy5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHR2YXIgaXRlbSA9IG1vZHVsZXNbaV07XHJcblx0XHRcdC8vIHNraXAgYWxyZWFkeSBpbXBvcnRlZCBtb2R1bGVcclxuXHRcdFx0Ly8gdGhpcyBpbXBsZW1lbnRhdGlvbiBpcyBub3QgMTAwJSBwZXJmZWN0IGZvciB3ZWlyZCBtZWRpYSBxdWVyeSBjb21iaW5hdGlvbnNcclxuXHRcdFx0Ly8gIHdoZW4gYSBtb2R1bGUgaXMgaW1wb3J0ZWQgbXVsdGlwbGUgdGltZXMgd2l0aCBkaWZmZXJlbnQgbWVkaWEgcXVlcmllcy5cclxuXHRcdFx0Ly8gIEkgaG9wZSB0aGlzIHdpbGwgbmV2ZXIgb2NjdXIgKEhleSB0aGlzIHdheSB3ZSBoYXZlIHNtYWxsZXIgYnVuZGxlcylcclxuXHRcdFx0aWYodHlwZW9mIGl0ZW1bMF0gIT09IFwibnVtYmVyXCIgfHwgIWFscmVhZHlJbXBvcnRlZE1vZHVsZXNbaXRlbVswXV0pIHtcclxuXHRcdFx0XHRpZihtZWRpYVF1ZXJ5ICYmICFpdGVtWzJdKSB7XHJcblx0XHRcdFx0XHRpdGVtWzJdID0gbWVkaWFRdWVyeTtcclxuXHRcdFx0XHR9IGVsc2UgaWYobWVkaWFRdWVyeSkge1xyXG5cdFx0XHRcdFx0aXRlbVsyXSA9IFwiKFwiICsgaXRlbVsyXSArIFwiKSBhbmQgKFwiICsgbWVkaWFRdWVyeSArIFwiKVwiO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRsaXN0LnB1c2goaXRlbSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9O1xyXG5cdHJldHVybiBsaXN0O1xyXG59O1xyXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcbi8vIG1vZHVsZSBpZCA9IDRcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiLypcblx0TUlUIExpY2Vuc2UgaHR0cDovL3d3dy5vcGVuc291cmNlLm9yZy9saWNlbnNlcy9taXQtbGljZW5zZS5waHBcblx0QXV0aG9yIFRvYmlhcyBLb3BwZXJzIEBzb2tyYVxuKi9cbnZhciBzdHlsZXNJbkRvbSA9IHt9LFxuXHRtZW1vaXplID0gZnVuY3Rpb24oZm4pIHtcblx0XHR2YXIgbWVtbztcblx0XHRyZXR1cm4gZnVuY3Rpb24gKCkge1xuXHRcdFx0aWYgKHR5cGVvZiBtZW1vID09PSBcInVuZGVmaW5lZFwiKSBtZW1vID0gZm4uYXBwbHkodGhpcywgYXJndW1lbnRzKTtcblx0XHRcdHJldHVybiBtZW1vO1xuXHRcdH07XG5cdH0sXG5cdGlzT2xkSUUgPSBtZW1vaXplKGZ1bmN0aW9uKCkge1xuXHRcdHJldHVybiAvbXNpZSBbNi05XVxcYi8udGVzdChzZWxmLm5hdmlnYXRvci51c2VyQWdlbnQudG9Mb3dlckNhc2UoKSk7XG5cdH0pLFxuXHRnZXRIZWFkRWxlbWVudCA9IG1lbW9pemUoZnVuY3Rpb24gKCkge1xuXHRcdHJldHVybiBkb2N1bWVudC5oZWFkIHx8IGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKFwiaGVhZFwiKVswXTtcblx0fSksXG5cdHNpbmdsZXRvbkVsZW1lbnQgPSBudWxsLFxuXHRzaW5nbGV0b25Db3VudGVyID0gMCxcblx0c3R5bGVFbGVtZW50c0luc2VydGVkQXRUb3AgPSBbXTtcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbihsaXN0LCBvcHRpb25zKSB7XG5cdGlmKHR5cGVvZiBERUJVRyAhPT0gXCJ1bmRlZmluZWRcIiAmJiBERUJVRykge1xuXHRcdGlmKHR5cGVvZiBkb2N1bWVudCAhPT0gXCJvYmplY3RcIikgdGhyb3cgbmV3IEVycm9yKFwiVGhlIHN0eWxlLWxvYWRlciBjYW5ub3QgYmUgdXNlZCBpbiBhIG5vbi1icm93c2VyIGVudmlyb25tZW50XCIpO1xuXHR9XG5cblx0b3B0aW9ucyA9IG9wdGlvbnMgfHwge307XG5cdC8vIEZvcmNlIHNpbmdsZS10YWcgc29sdXRpb24gb24gSUU2LTksIHdoaWNoIGhhcyBhIGhhcmQgbGltaXQgb24gdGhlICMgb2YgPHN0eWxlPlxuXHQvLyB0YWdzIGl0IHdpbGwgYWxsb3cgb24gYSBwYWdlXG5cdGlmICh0eXBlb2Ygb3B0aW9ucy5zaW5nbGV0b24gPT09IFwidW5kZWZpbmVkXCIpIG9wdGlvbnMuc2luZ2xldG9uID0gaXNPbGRJRSgpO1xuXG5cdC8vIEJ5IGRlZmF1bHQsIGFkZCA8c3R5bGU+IHRhZ3MgdG8gdGhlIGJvdHRvbSBvZiA8aGVhZD4uXG5cdGlmICh0eXBlb2Ygb3B0aW9ucy5pbnNlcnRBdCA9PT0gXCJ1bmRlZmluZWRcIikgb3B0aW9ucy5pbnNlcnRBdCA9IFwiYm90dG9tXCI7XG5cblx0dmFyIHN0eWxlcyA9IGxpc3RUb1N0eWxlcyhsaXN0KTtcblx0YWRkU3R5bGVzVG9Eb20oc3R5bGVzLCBvcHRpb25zKTtcblxuXHRyZXR1cm4gZnVuY3Rpb24gdXBkYXRlKG5ld0xpc3QpIHtcblx0XHR2YXIgbWF5UmVtb3ZlID0gW107XG5cdFx0Zm9yKHZhciBpID0gMDsgaSA8IHN0eWxlcy5sZW5ndGg7IGkrKykge1xuXHRcdFx0dmFyIGl0ZW0gPSBzdHlsZXNbaV07XG5cdFx0XHR2YXIgZG9tU3R5bGUgPSBzdHlsZXNJbkRvbVtpdGVtLmlkXTtcblx0XHRcdGRvbVN0eWxlLnJlZnMtLTtcblx0XHRcdG1heVJlbW92ZS5wdXNoKGRvbVN0eWxlKTtcblx0XHR9XG5cdFx0aWYobmV3TGlzdCkge1xuXHRcdFx0dmFyIG5ld1N0eWxlcyA9IGxpc3RUb1N0eWxlcyhuZXdMaXN0KTtcblx0XHRcdGFkZFN0eWxlc1RvRG9tKG5ld1N0eWxlcywgb3B0aW9ucyk7XG5cdFx0fVxuXHRcdGZvcih2YXIgaSA9IDA7IGkgPCBtYXlSZW1vdmUubGVuZ3RoOyBpKyspIHtcblx0XHRcdHZhciBkb21TdHlsZSA9IG1heVJlbW92ZVtpXTtcblx0XHRcdGlmKGRvbVN0eWxlLnJlZnMgPT09IDApIHtcblx0XHRcdFx0Zm9yKHZhciBqID0gMDsgaiA8IGRvbVN0eWxlLnBhcnRzLmxlbmd0aDsgaisrKVxuXHRcdFx0XHRcdGRvbVN0eWxlLnBhcnRzW2pdKCk7XG5cdFx0XHRcdGRlbGV0ZSBzdHlsZXNJbkRvbVtkb21TdHlsZS5pZF07XG5cdFx0XHR9XG5cdFx0fVxuXHR9O1xufVxuXG5mdW5jdGlvbiBhZGRTdHlsZXNUb0RvbShzdHlsZXMsIG9wdGlvbnMpIHtcblx0Zm9yKHZhciBpID0gMDsgaSA8IHN0eWxlcy5sZW5ndGg7IGkrKykge1xuXHRcdHZhciBpdGVtID0gc3R5bGVzW2ldO1xuXHRcdHZhciBkb21TdHlsZSA9IHN0eWxlc0luRG9tW2l0ZW0uaWRdO1xuXHRcdGlmKGRvbVN0eWxlKSB7XG5cdFx0XHRkb21TdHlsZS5yZWZzKys7XG5cdFx0XHRmb3IodmFyIGogPSAwOyBqIDwgZG9tU3R5bGUucGFydHMubGVuZ3RoOyBqKyspIHtcblx0XHRcdFx0ZG9tU3R5bGUucGFydHNbal0oaXRlbS5wYXJ0c1tqXSk7XG5cdFx0XHR9XG5cdFx0XHRmb3IoOyBqIDwgaXRlbS5wYXJ0cy5sZW5ndGg7IGorKykge1xuXHRcdFx0XHRkb21TdHlsZS5wYXJ0cy5wdXNoKGFkZFN0eWxlKGl0ZW0ucGFydHNbal0sIG9wdGlvbnMpKTtcblx0XHRcdH1cblx0XHR9IGVsc2Uge1xuXHRcdFx0dmFyIHBhcnRzID0gW107XG5cdFx0XHRmb3IodmFyIGogPSAwOyBqIDwgaXRlbS5wYXJ0cy5sZW5ndGg7IGorKykge1xuXHRcdFx0XHRwYXJ0cy5wdXNoKGFkZFN0eWxlKGl0ZW0ucGFydHNbal0sIG9wdGlvbnMpKTtcblx0XHRcdH1cblx0XHRcdHN0eWxlc0luRG9tW2l0ZW0uaWRdID0ge2lkOiBpdGVtLmlkLCByZWZzOiAxLCBwYXJ0czogcGFydHN9O1xuXHRcdH1cblx0fVxufVxuXG5mdW5jdGlvbiBsaXN0VG9TdHlsZXMobGlzdCkge1xuXHR2YXIgc3R5bGVzID0gW107XG5cdHZhciBuZXdTdHlsZXMgPSB7fTtcblx0Zm9yKHZhciBpID0gMDsgaSA8IGxpc3QubGVuZ3RoOyBpKyspIHtcblx0XHR2YXIgaXRlbSA9IGxpc3RbaV07XG5cdFx0dmFyIGlkID0gaXRlbVswXTtcblx0XHR2YXIgY3NzID0gaXRlbVsxXTtcblx0XHR2YXIgbWVkaWEgPSBpdGVtWzJdO1xuXHRcdHZhciBzb3VyY2VNYXAgPSBpdGVtWzNdO1xuXHRcdHZhciBwYXJ0ID0ge2NzczogY3NzLCBtZWRpYTogbWVkaWEsIHNvdXJjZU1hcDogc291cmNlTWFwfTtcblx0XHRpZighbmV3U3R5bGVzW2lkXSlcblx0XHRcdHN0eWxlcy5wdXNoKG5ld1N0eWxlc1tpZF0gPSB7aWQ6IGlkLCBwYXJ0czogW3BhcnRdfSk7XG5cdFx0ZWxzZVxuXHRcdFx0bmV3U3R5bGVzW2lkXS5wYXJ0cy5wdXNoKHBhcnQpO1xuXHR9XG5cdHJldHVybiBzdHlsZXM7XG59XG5cbmZ1bmN0aW9uIGluc2VydFN0eWxlRWxlbWVudChvcHRpb25zLCBzdHlsZUVsZW1lbnQpIHtcblx0dmFyIGhlYWQgPSBnZXRIZWFkRWxlbWVudCgpO1xuXHR2YXIgbGFzdFN0eWxlRWxlbWVudEluc2VydGVkQXRUb3AgPSBzdHlsZUVsZW1lbnRzSW5zZXJ0ZWRBdFRvcFtzdHlsZUVsZW1lbnRzSW5zZXJ0ZWRBdFRvcC5sZW5ndGggLSAxXTtcblx0aWYgKG9wdGlvbnMuaW5zZXJ0QXQgPT09IFwidG9wXCIpIHtcblx0XHRpZighbGFzdFN0eWxlRWxlbWVudEluc2VydGVkQXRUb3ApIHtcblx0XHRcdGhlYWQuaW5zZXJ0QmVmb3JlKHN0eWxlRWxlbWVudCwgaGVhZC5maXJzdENoaWxkKTtcblx0XHR9IGVsc2UgaWYobGFzdFN0eWxlRWxlbWVudEluc2VydGVkQXRUb3AubmV4dFNpYmxpbmcpIHtcblx0XHRcdGhlYWQuaW5zZXJ0QmVmb3JlKHN0eWxlRWxlbWVudCwgbGFzdFN0eWxlRWxlbWVudEluc2VydGVkQXRUb3AubmV4dFNpYmxpbmcpO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHRoZWFkLmFwcGVuZENoaWxkKHN0eWxlRWxlbWVudCk7XG5cdFx0fVxuXHRcdHN0eWxlRWxlbWVudHNJbnNlcnRlZEF0VG9wLnB1c2goc3R5bGVFbGVtZW50KTtcblx0fSBlbHNlIGlmIChvcHRpb25zLmluc2VydEF0ID09PSBcImJvdHRvbVwiKSB7XG5cdFx0aGVhZC5hcHBlbmRDaGlsZChzdHlsZUVsZW1lbnQpO1xuXHR9IGVsc2Uge1xuXHRcdHRocm93IG5ldyBFcnJvcihcIkludmFsaWQgdmFsdWUgZm9yIHBhcmFtZXRlciAnaW5zZXJ0QXQnLiBNdXN0IGJlICd0b3AnIG9yICdib3R0b20nLlwiKTtcblx0fVxufVxuXG5mdW5jdGlvbiByZW1vdmVTdHlsZUVsZW1lbnQoc3R5bGVFbGVtZW50KSB7XG5cdHN0eWxlRWxlbWVudC5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKHN0eWxlRWxlbWVudCk7XG5cdHZhciBpZHggPSBzdHlsZUVsZW1lbnRzSW5zZXJ0ZWRBdFRvcC5pbmRleE9mKHN0eWxlRWxlbWVudCk7XG5cdGlmKGlkeCA+PSAwKSB7XG5cdFx0c3R5bGVFbGVtZW50c0luc2VydGVkQXRUb3Auc3BsaWNlKGlkeCwgMSk7XG5cdH1cbn1cblxuZnVuY3Rpb24gY3JlYXRlU3R5bGVFbGVtZW50KG9wdGlvbnMpIHtcblx0dmFyIHN0eWxlRWxlbWVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJzdHlsZVwiKTtcblx0c3R5bGVFbGVtZW50LnR5cGUgPSBcInRleHQvY3NzXCI7XG5cdGluc2VydFN0eWxlRWxlbWVudChvcHRpb25zLCBzdHlsZUVsZW1lbnQpO1xuXHRyZXR1cm4gc3R5bGVFbGVtZW50O1xufVxuXG5mdW5jdGlvbiBjcmVhdGVMaW5rRWxlbWVudChvcHRpb25zKSB7XG5cdHZhciBsaW5rRWxlbWVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJsaW5rXCIpO1xuXHRsaW5rRWxlbWVudC5yZWwgPSBcInN0eWxlc2hlZXRcIjtcblx0aW5zZXJ0U3R5bGVFbGVtZW50KG9wdGlvbnMsIGxpbmtFbGVtZW50KTtcblx0cmV0dXJuIGxpbmtFbGVtZW50O1xufVxuXG5mdW5jdGlvbiBhZGRTdHlsZShvYmosIG9wdGlvbnMpIHtcblx0dmFyIHN0eWxlRWxlbWVudCwgdXBkYXRlLCByZW1vdmU7XG5cblx0aWYgKG9wdGlvbnMuc2luZ2xldG9uKSB7XG5cdFx0dmFyIHN0eWxlSW5kZXggPSBzaW5nbGV0b25Db3VudGVyKys7XG5cdFx0c3R5bGVFbGVtZW50ID0gc2luZ2xldG9uRWxlbWVudCB8fCAoc2luZ2xldG9uRWxlbWVudCA9IGNyZWF0ZVN0eWxlRWxlbWVudChvcHRpb25zKSk7XG5cdFx0dXBkYXRlID0gYXBwbHlUb1NpbmdsZXRvblRhZy5iaW5kKG51bGwsIHN0eWxlRWxlbWVudCwgc3R5bGVJbmRleCwgZmFsc2UpO1xuXHRcdHJlbW92ZSA9IGFwcGx5VG9TaW5nbGV0b25UYWcuYmluZChudWxsLCBzdHlsZUVsZW1lbnQsIHN0eWxlSW5kZXgsIHRydWUpO1xuXHR9IGVsc2UgaWYob2JqLnNvdXJjZU1hcCAmJlxuXHRcdHR5cGVvZiBVUkwgPT09IFwiZnVuY3Rpb25cIiAmJlxuXHRcdHR5cGVvZiBVUkwuY3JlYXRlT2JqZWN0VVJMID09PSBcImZ1bmN0aW9uXCIgJiZcblx0XHR0eXBlb2YgVVJMLnJldm9rZU9iamVjdFVSTCA9PT0gXCJmdW5jdGlvblwiICYmXG5cdFx0dHlwZW9mIEJsb2IgPT09IFwiZnVuY3Rpb25cIiAmJlxuXHRcdHR5cGVvZiBidG9hID09PSBcImZ1bmN0aW9uXCIpIHtcblx0XHRzdHlsZUVsZW1lbnQgPSBjcmVhdGVMaW5rRWxlbWVudChvcHRpb25zKTtcblx0XHR1cGRhdGUgPSB1cGRhdGVMaW5rLmJpbmQobnVsbCwgc3R5bGVFbGVtZW50KTtcblx0XHRyZW1vdmUgPSBmdW5jdGlvbigpIHtcblx0XHRcdHJlbW92ZVN0eWxlRWxlbWVudChzdHlsZUVsZW1lbnQpO1xuXHRcdFx0aWYoc3R5bGVFbGVtZW50LmhyZWYpXG5cdFx0XHRcdFVSTC5yZXZva2VPYmplY3RVUkwoc3R5bGVFbGVtZW50LmhyZWYpO1xuXHRcdH07XG5cdH0gZWxzZSB7XG5cdFx0c3R5bGVFbGVtZW50ID0gY3JlYXRlU3R5bGVFbGVtZW50KG9wdGlvbnMpO1xuXHRcdHVwZGF0ZSA9IGFwcGx5VG9UYWcuYmluZChudWxsLCBzdHlsZUVsZW1lbnQpO1xuXHRcdHJlbW92ZSA9IGZ1bmN0aW9uKCkge1xuXHRcdFx0cmVtb3ZlU3R5bGVFbGVtZW50KHN0eWxlRWxlbWVudCk7XG5cdFx0fTtcblx0fVxuXG5cdHVwZGF0ZShvYmopO1xuXG5cdHJldHVybiBmdW5jdGlvbiB1cGRhdGVTdHlsZShuZXdPYmopIHtcblx0XHRpZihuZXdPYmopIHtcblx0XHRcdGlmKG5ld09iai5jc3MgPT09IG9iai5jc3MgJiYgbmV3T2JqLm1lZGlhID09PSBvYmoubWVkaWEgJiYgbmV3T2JqLnNvdXJjZU1hcCA9PT0gb2JqLnNvdXJjZU1hcClcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0dXBkYXRlKG9iaiA9IG5ld09iaik7XG5cdFx0fSBlbHNlIHtcblx0XHRcdHJlbW92ZSgpO1xuXHRcdH1cblx0fTtcbn1cblxudmFyIHJlcGxhY2VUZXh0ID0gKGZ1bmN0aW9uICgpIHtcblx0dmFyIHRleHRTdG9yZSA9IFtdO1xuXG5cdHJldHVybiBmdW5jdGlvbiAoaW5kZXgsIHJlcGxhY2VtZW50KSB7XG5cdFx0dGV4dFN0b3JlW2luZGV4XSA9IHJlcGxhY2VtZW50O1xuXHRcdHJldHVybiB0ZXh0U3RvcmUuZmlsdGVyKEJvb2xlYW4pLmpvaW4oJ1xcbicpO1xuXHR9O1xufSkoKTtcblxuZnVuY3Rpb24gYXBwbHlUb1NpbmdsZXRvblRhZyhzdHlsZUVsZW1lbnQsIGluZGV4LCByZW1vdmUsIG9iaikge1xuXHR2YXIgY3NzID0gcmVtb3ZlID8gXCJcIiA6IG9iai5jc3M7XG5cblx0aWYgKHN0eWxlRWxlbWVudC5zdHlsZVNoZWV0KSB7XG5cdFx0c3R5bGVFbGVtZW50LnN0eWxlU2hlZXQuY3NzVGV4dCA9IHJlcGxhY2VUZXh0KGluZGV4LCBjc3MpO1xuXHR9IGVsc2Uge1xuXHRcdHZhciBjc3NOb2RlID0gZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUoY3NzKTtcblx0XHR2YXIgY2hpbGROb2RlcyA9IHN0eWxlRWxlbWVudC5jaGlsZE5vZGVzO1xuXHRcdGlmIChjaGlsZE5vZGVzW2luZGV4XSkgc3R5bGVFbGVtZW50LnJlbW92ZUNoaWxkKGNoaWxkTm9kZXNbaW5kZXhdKTtcblx0XHRpZiAoY2hpbGROb2Rlcy5sZW5ndGgpIHtcblx0XHRcdHN0eWxlRWxlbWVudC5pbnNlcnRCZWZvcmUoY3NzTm9kZSwgY2hpbGROb2Rlc1tpbmRleF0pO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHRzdHlsZUVsZW1lbnQuYXBwZW5kQ2hpbGQoY3NzTm9kZSk7XG5cdFx0fVxuXHR9XG59XG5cbmZ1bmN0aW9uIGFwcGx5VG9UYWcoc3R5bGVFbGVtZW50LCBvYmopIHtcblx0dmFyIGNzcyA9IG9iai5jc3M7XG5cdHZhciBtZWRpYSA9IG9iai5tZWRpYTtcblxuXHRpZihtZWRpYSkge1xuXHRcdHN0eWxlRWxlbWVudC5zZXRBdHRyaWJ1dGUoXCJtZWRpYVwiLCBtZWRpYSlcblx0fVxuXG5cdGlmKHN0eWxlRWxlbWVudC5zdHlsZVNoZWV0KSB7XG5cdFx0c3R5bGVFbGVtZW50LnN0eWxlU2hlZXQuY3NzVGV4dCA9IGNzcztcblx0fSBlbHNlIHtcblx0XHR3aGlsZShzdHlsZUVsZW1lbnQuZmlyc3RDaGlsZCkge1xuXHRcdFx0c3R5bGVFbGVtZW50LnJlbW92ZUNoaWxkKHN0eWxlRWxlbWVudC5maXJzdENoaWxkKTtcblx0XHR9XG5cdFx0c3R5bGVFbGVtZW50LmFwcGVuZENoaWxkKGRvY3VtZW50LmNyZWF0ZVRleHROb2RlKGNzcykpO1xuXHR9XG59XG5cbmZ1bmN0aW9uIHVwZGF0ZUxpbmsobGlua0VsZW1lbnQsIG9iaikge1xuXHR2YXIgY3NzID0gb2JqLmNzcztcblx0dmFyIHNvdXJjZU1hcCA9IG9iai5zb3VyY2VNYXA7XG5cblx0aWYoc291cmNlTWFwKSB7XG5cdFx0Ly8gaHR0cDovL3N0YWNrb3ZlcmZsb3cuY29tL2EvMjY2MDM4NzVcblx0XHRjc3MgKz0gXCJcXG4vKiMgc291cmNlTWFwcGluZ1VSTD1kYXRhOmFwcGxpY2F0aW9uL2pzb247YmFzZTY0LFwiICsgYnRvYSh1bmVzY2FwZShlbmNvZGVVUklDb21wb25lbnQoSlNPTi5zdHJpbmdpZnkoc291cmNlTWFwKSkpKSArIFwiICovXCI7XG5cdH1cblxuXHR2YXIgYmxvYiA9IG5ldyBCbG9iKFtjc3NdLCB7IHR5cGU6IFwidGV4dC9jc3NcIiB9KTtcblxuXHR2YXIgb2xkU3JjID0gbGlua0VsZW1lbnQuaHJlZjtcblxuXHRsaW5rRWxlbWVudC5ocmVmID0gVVJMLmNyZWF0ZU9iamVjdFVSTChibG9iKTtcblxuXHRpZihvbGRTcmMpXG5cdFx0VVJMLnJldm9rZU9iamVjdFVSTChvbGRTcmMpO1xufVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3N0eWxlLWxvYWRlci9hZGRTdHlsZXMuanNcbi8vIG1vZHVsZSBpZCA9IDVcbi8vIG1vZHVsZSBjaHVua3MgPSAwIl0sInNvdXJjZVJvb3QiOiIifQ==